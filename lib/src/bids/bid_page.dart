import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BidPage extends StatefulWidget {
  BidPage(this.id, this.last_price, this.user);

  String id;
  String last_price;
  User user;

  @override
  _BidPageState createState() => _BidPageState();
}

class _BidPageState extends State<BidPage> {
  bool show = false;
  RestService rest = new RestService();

  var price = 0.0;
  var _value = 0.0;
  var _startprice = TextEditingController();
  var price_init = 0.0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  get_price() {
    FirebaseDatabase.instance
        .reference()
        .child("last_price")
        .child(widget.id)
        .onValue
        .listen((val) {
      var d;
      // try {

      if (val.snapshot.value.toString() == "null") {
        setState(() {
          price = double.parse(widget.last_price);
        });
      } else {

        if (this.mounted) {
          setState(() {
            d = val.snapshot.value;
            price = val.snapshot.value["price"];
            price_init = val.snapshot.value["price"];
            _value = price;
            _startprice.text = _value.toString();

          });
        }
      }
      /*  } catch (e) {}*/
    });
  }

  @override
  void initState() {
    super.initState();

    get_price();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            children: [
              Center(child: text("Place your Bid")),
              Container(
                height: 10.h,
              ),
              Center(
                  child: text2(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
                "sed do eiusmod tempor incididunt.",
              )),
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xffF7F6F9),
                  ),
                  width: MediaQuery.of(context).size.width * 0.94,
                  child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Column(children: [
                        Container(
                            //  width: MediaQuery.of(context).size.width * 0.9,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                              Text(
                                "Current Bid : ".toUpperCase(),
                                style: TextStyle(
                                    color: Color(0xffA3AFD5),
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(
                                "${price.toStringAsFixed(2)}KD ",
                                style: TextStyle(
                                    color: Config.col_blue,
                                    fontWeight: FontWeight.bold),
                              )
                            ])),
                        Container(
                          height: 20.h,
                        ),
                        /* Text(
                          _value.toStringAsFixed(2),
                          style: TextStyle(
                            fontSize: 39.sp,
                            fontFamily: "Roboto Condensed",
                            fontWeight: FontWeight.w900,
                          ),
                        ),*/

                        Container(
                            width: 200.w,
                            child: TextFormField(
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.number,
                              controller: _startprice,
                              style: TextStyle(
                                fontSize: 26.sp,
                                fontFamily: "Roboto Condensed",
                                fontWeight: FontWeight.w900,
                              ),
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  hintStyle: TextStyle(
                                    fontSize: 32.sp,
                                    fontFamily: "Roboto Condensed",
                                    fontWeight: FontWeight.w900,
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 32.sp,
                                    fontFamily: "Roboto Condensed",
                                    fontWeight: FontWeight.w900,
                                  ),
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  contentPadding:
                                      EdgeInsets.only(left: 16.w, right: 16.w),
                                  hintText: "0.00"),
                            )),
                        Container(
                          height: 8.h,
                        ),
                      ]))),
              Container(
                height: 12.h,
              ),
              Center(
                  child: Text(
                "Your minimum bid cannot be inferior to ${price.toStringAsFixed(2)} KD",
                style: TextStyle(color: Config.col_red),
              )),
              Container(
                height: 24.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: ScreenUtil().setWidth(168),
                      height: ScreenUtil().setHeight(46),
                      child: RaisedButton(
                          color: Config.col_blue,
                          elevation: 1,
                          shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(28),
                            ),
                          ),
                          onPressed: () async {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            String id = prefs.getString("id");

                            if (double.parse(_startprice.text) > price) {
                              setState(() {
                                show = true;
                                _value = double.parse(_startprice.text);
                                price = double.parse(_startprice.text);
                              });
                              await FirebaseDatabase.instance
                                  .reference()
                                  .child("last_price")
                                  .child(widget.id)
                                  .update({
                                "name": widget.user.userename,
                                "avatar": widget.user.image,
                                "id": widget.user.id,
                                "price": double.parse(_startprice.text) + 0.00
                              });

                              var a = await rest.post(
                                  "updateLastPrice/" + widget.id,
                                  json.encode({
                                    "price":
                                        double.parse(_startprice.text) + 0.01,
                                    "userid": int.parse(id)
                                  }));



                              setState(() {
                                show = false;
                              });
                            } else {
                              _scaffoldKey.currentState
                                  .showSnackBar(new SnackBar(
                                backgroundColor: Config.col_red,
                                content: new Text(
                                  "Your minimum bid cannot be inferior to ${double.parse(widget.last_price).toStringAsFixed(2)} KD",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18.sp),
                                ),
                              ));
                            }
                            /* Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MainScreen()),
                                    (Route<dynamic> route) => false);*/
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? CupertinoTheme(
                                      data: CupertinoTheme.of(context).copyWith(
                                          brightness: Brightness.dark),
                                      child: CupertinoActivityIndicator())
                                  : Container(),
                              show //CupertinoActivityIndicator()
                                  ? Container(
                                      width: 4,
                                    )
                                  : Container(),
                              Text(
                                "CONFIRM BID",
                                style: text_btn2,
                              ),
                            ],
                          ))),
                ],
              ),
              Container(
                height: 32.h,
              ),
              SfSlider(
                min: price,
                max: price + 40.0,
                value: _value,
                interval: 20,

                minorTicksPerInterval: 10,
                // showTicks: true,
                showLabels: true,
                enableTooltip: true,
                // minorTicksPerInterval: 1,
                onChanged: (dynamic value) {
                  setState(() {
                    _value = value;
                    _startprice.text = _value.toStringAsFixed(2);
                  });
                },
              ),
            ]));
  }
}

import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/models/store.dart';
import 'package:meta/meta.dart';

class HomeResponse {
  final int responseCode;
  final String message;
  final List<Store> stores;
  final List<Category> categories;
  final List<Product> bids;
  final List<Product> products;

  HomeResponse({
    @required this.responseCode,
    @required this.message,
    @required this.stores,
    @required this.products,
    @required this.categories,
    @required this.bids,
  });
}

import 'package:meta/meta.dart';

class LNotification {
  final String id;
  final String image;
  final String name;
  final String time;

  final String user;

  LNotification(
      {@required this.id, this.name, this.image, this.user, this.time});

  factory LNotification.fromMap(Map<String, dynamic> map) {
    return LNotification(
        id: map['id'] as String,
        image: map['image'] as String,
        name: map['name'] as String,
        user: map['user'] as String,
        time: map['time'] as String);
  }

  static List<LNotification> notif_list = [
    {
      "name": "You have received new bid form",
      "image": "",
      "id": "1",
      "time": "Yesterday",
      "user": "Adam Daaif"
    },
    {
      "name": "Monday is the deals day, Adam. New promotions for you",
      "time": "Feb 14",
      "image": "",
      "id": "3287"
    },
    {
      "name": "Monday is the deals day, Adam. New promotions for you",
      "time": "Feb 14",
      "image": "",
      "id": "73993"
    },
  ].map((e) => LNotification.fromMap(e)).toList();
}

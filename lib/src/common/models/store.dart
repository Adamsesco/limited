import 'package:limited/src/common/models/product.dart';
import 'package:meta/meta.dart';

class Store {
  final String id;
  final String image;
  final String name;
  String addresess;
  DateTime created;
  List<String> images;
  List bids;
  List products;

  Store(
      {@required this.id,
      this.name,
      this.image,
      this.addresess,
      this.created,
      this.bids,
      this.products,
      this.images});

  factory Store.fromMap(Map<String, dynamic> map) {
    return Store(
        id: map['id'] as String,
        image: map['avatar'] == null
            ? ''
            : map['avatar'] as String,
        images: map['sliders'] == null
            ? [
                '',
                '',
                ''
              ]
            : List<String>.from(map['sliders']),
        name: map['username'] as String,
        bids: map['bids'] == null
            ? List<Product>.from([])
            : map['bids'].map((e) => Product.fromMap(e)).toList(),
        products: map['products'] == null
            ? List<Product>.from([])
            : map['products'].map((e) => Product.fromMap(e)).toList(),
        created:
            map["date"] == null ? DateTime.now() : DateTime.parse(map["date"]),
        addresess: map["addresess"]);
  }

/*static List<Store> stores_list = [
    {
      "name": "Craftizen",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/07a260fd6b341dadd537752f2660c672_w5xilh.png",
      "id": "2"
    },
    {
      "name": "Flower Eden",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/15809fdd9d1524b2a40ea4a62b89e780_zx1hpk.png",
      "id": "3"
    },
    {
      "name": "Craftizen",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/07a260fd6b341dadd537752f2660c672_w5xilh.png",
      "id": "2"
    },
    {
      "name": "Flower Eden",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/15809fdd9d1524b2a40ea4a62b89e780_zx1hpk.png",
      "id": "3"
    },
    {
      "name": "Craftizen",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/07a260fd6b341dadd537752f2660c672_w5xilh.png",
      "id": "2"
    },
    {
      "name": "Flower Eden",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615321412/15809fdd9d1524b2a40ea4a62b89e780_zx1hpk.png",
      "id": "3"
    }
  ].map((e) => Store.fromMap(e)).toList();*/
}

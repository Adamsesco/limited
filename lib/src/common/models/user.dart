class User {
  String id;
  String name;
  String image;
  String token;
  String email;
  String addresess;
  String mobile;
  String userename;
  String login_token;
  DateTime created;
  var password;
  var type;

  bool isActive ;



  User(this.id, this.name, this.image, this.token,this.created,this.mobile,this.userename);

  User.fromMap(Map<String, dynamic> map)
      : id = map["id"],
        isActive = false,
        name = map["username"],
        type = map["type"],
      password = map["password"],
        created =map["date"] == null?DateTime.now(): DateTime.parse(map["date"]),
        image = map["avatar"],
        userename = map["username"],

      email = map["email"],
        token = map["device_token"],
        mobile = map["phone"],
        login_token = map["token"],
        addresess = map["addresess"];
}

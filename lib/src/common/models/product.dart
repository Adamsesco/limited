import 'package:limited/src/common/models/category.dart';
import 'package:meta/meta.dart';

class Product {
  final String id;
  final List<String> image;
  final List<String> images;

  var expired_date;
  String category_id;
  final String name;
  final String description;
  String category_name_en;
  String type;
  String avatar;
  String user_id;
  String username;
  String city;

  String last_price;
  String time;
  String category;
  String l_desc;

  Product(
      {@required this.id,
      this.name,
      this.image,
      this.type,
      this.category,
      this.username,
      this.images,
      this.city,
      this.category_id,
      this.expired_date,
      this.category_name_en,
      this.l_desc,
      this.description,
      this.user_id,
      this.avatar,
      this.last_price,
      this.time});

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      id: map['id'] as String,
      city: map['city'] as String,
      category_id: map['category_id'] as String,
      category_name_en: map['category_name_en'] as String,
      image: map.containsKey('image') == true
          ? map['image'] == null
              ? [
                  ""
                ]
              : List<String>.from(map['image'])
          : map['images'] == null
              ? [
                  ""
                ]
              : List<String>.from(map['images']),
      images: map.containsKey('images') == false
          ? [
              ''
            ]
          : map['images'] == null
              ? [""]
              : List<String>.from(map['images']),
      l_desc: map['description'] as String,
      name: map['title'] as String,
      user_id: map['user_id'] as String,
      avatar: map['avatar'] as String,
      type: map['type'] as String,
      time: map['date'] as String,
      description: map['subtitle'] as String,
      last_price: map['starting_price'].toString() == "null"
          ? map['price'].toString()
          : map['starting_price'].toString() as String,
      category: map['category'] as String,
      username: map['username'] as String,
      expired_date: map['expired_date'] == null
          ? null
          : DateTime.parse(map['expired_date']),
    );
  }

}

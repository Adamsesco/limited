import 'package:meta/meta.dart';

class Category {
  final String id;
  final String image;
  final String name;
  bool check;
  final String totalads;

  Category(
      {@required this.id, this.name, this.image, this.totalads, this.check});

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
        check: false,
        id: map['id'] as String,
        image: map['image'] as String,
        name: map['name'] as String,
        totalads: map['totalads'] as String);
  }
/*
  static List<Category> categories_list = [
    {
      "name": "Antiques",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/Groupe_de_masques_1_d0f0ls.png",
      "id": "1"
    },
    {
      "name": "Accessories",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/Groupe_de_masques_2_w9rknf.png",
      "id": "3287"
    },
    {
      "name": "Decorations",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/Groupe_de_masques_3_geqi5j.png",
      "id": "73993"
    },
    {
      "name": "Clothes",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/Groupe_de_masques_4_sqnytd.png",
      "id": "234"
    },
    {
      "name": "Cakes",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/Groupe_de_masques_5_bfevzk.png",
      "id": "99995"
    },
    {
      "name": "Pottery",
      "image":
          "https://res.cloudinary.com/dgxctjlpx/image/upload/v1615062895/722148f64fea8731113fd32f8866def1_ekyf2o.png",
      "id": "30006"
    }
  ].map((e) => Category.fromMap(e)).toList();*/
}

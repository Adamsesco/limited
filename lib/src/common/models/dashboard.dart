import 'package:limited/src/common/models/product.dart';
import 'package:meta/meta.dart';

class Dashboard1 {
  var soldedItems;
  var totalPayments;
  var activeItems;
  var unactiveItems;
  List<Product> products;

  Dashboard1(
      {this.products,
      this.activeItems,
      this.soldedItems,
      this.totalPayments,
      this.unactiveItems});

  factory Dashboard1.fromMap(map) {
    return Dashboard1(
      products: List<Product>.from(map["items"].map((a) => Product.fromMap(a)).toList()),
      activeItems: map["activeItems"].toString(),
      soldedItems: map["soldedItems"].toString(),
      totalPayments: map["totalPayments"].toString(),
      unactiveItems: map["unactiveItems"].toString(),
    );
  }


}

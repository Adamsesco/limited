import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:limited/src/otp/otp_verification.dart';
import 'package:limited/src/signin/signin.dart';

class NotLogin extends StatefulWidget {
  NotLogin();

  @override
  _SubmissionState createState() => _SubmissionState();
}

class _SubmissionState extends State<NotLogin> {
  TextStyle text1_btn = TextStyle(
      color: Colors.white,
      fontSize: ScreenUtil().setSp(13),
      fontWeight: FontWeight.w700);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return new Stack(fit: StackFit.expand, children: <Widget>[
      Container(
          padding: const EdgeInsets.only(left: 0, right: 0, top: 8),
          child: new Container(
              padding: const EdgeInsets.only(top: 0.0),
              child: new Center(
                  child: new ListView(
                padding: const EdgeInsets.all(24.0),
                children: <Widget>[
                  new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                            child: SvgPicture.asset(
                          "assets/images/icons/logo.svg",
                          width: ScreenUtil().setWidth(120),
                          height: ScreenUtil().setHeight(120),
                        )),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                        ),
                        Center(
                            child: Text("pls_login".tr(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: ScreenUtil().setSp(17.5)))),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.7,
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.14,
                        ),
                        Center(
                            child: Container(
                                height: ScreenUtil().setHeight(42),
                                width: ScreenUtil().setWidth(212),
                                child: RaisedButton(
                                    color: Config.col_blue,
                                    elevation: 1,
                                    shape: new RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(28),
                                      ),
                                    ),
                                    onPressed: () {
                                      Navigator.push(context,
                                          new MaterialPageRoute(builder: (BuildContext context) {
                                            return OtpVerification();
                                          }));
                                    },
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "LOGIN",
                                            style: text1_btn,
                                          ),
                                        ])))),


                      ])
                ],
              ))))
    ]);
  }
}

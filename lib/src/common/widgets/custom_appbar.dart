import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/home/categories/categories_list.dart';

class LAppBar extends StatelessWidget implements PreferredSizeWidget {
  LAppBar(this.menu, {this.leading_widget, this.actions_widget});

  Widget leading_widget;
  Widget actions_widget;
  bool menu;

  @override
  Size get preferredSize => Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Color(0xff3E3E3E)
          //color: Config.col_green
          ),
      backgroundColor: Colors.white,
      elevation: 0,
      // padding: EdgeInsets.only(top: ScreenUtil().setHeight(22)),
      leading: leading_widget,
      title: Image.asset(
        "assets/images/icons/logo.png",
        width: 120.w,
      ),
      actions: [
        IconButton(
          icon: menu == false ? actions_widget : SvgPicture.asset(
              /*_index==4?"assets/images/icons/edit.svg":*/
              "assets/images/icons/menu.svg"),
          onPressed: () {
            Navigator.push(context,
                new MaterialPageRoute(builder: (BuildContext context) {
              return CategoriesList();
            }));
          },
        ),
      ],
    );
  }
}

/**
    Padding(
    padding: EdgeInsets.only(top: ScreenUtil().setHeight(22)),
    child: Row(
    // pinned: true,
    // snap: true,
    //floating: true,
    children: [
    SizedBox(
    width: ScreenUtil().setWidth(16.5),
    ),
    Padding(
    padding: EdgeInsets.only(top: ScreenUtil().setHeight(34)),
    child: leading_widget),
    Expanded(
    flex: 1,
    child: Padding(
    padding:
    EdgeInsets.all( ScreenUtil().setHeight(24)),
    child: Image.asset("assets/images/icons/logo.png"))),
    Padding(
    padding: EdgeInsets.only(top: ScreenUtil().setHeight(34)),
    child: IconButton(
    icon: menu == false
    ? actions_widget
    : SvgPicture.asset(
    /*_index==4?"assets/images/icons/edit.svg":*/
    "assets/images/icons/menu.svg"),
    onPressed: () {
    Navigator.push(context,
    new MaterialPageRoute(builder: (BuildContext context) {
    return CategoriesList();
    }));
    },
    ),
    ),
    SizedBox(width: ScreenUtil().setWidth(16.5)),
    ]))
 **/

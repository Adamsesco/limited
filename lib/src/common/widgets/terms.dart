
import 'package:flutter/material.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/services/rest_services.dart';

class Terms extends StatefulWidget {
  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  RestService rest = RestService();
  bool load = true;
  String text = "";

  get_terms() async {
    var a = await rest.get("terms");
    if (!this.mounted) return;

    setState(() {
      text = a["terms"];
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_terms();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Terms")),
      body: load
          ? Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Config.col_blue),
          ))
          : ListView(
        padding: EdgeInsets.all(16),
        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width * 0.9,
              child: Text(text,textDirection: TextDirection.ltr,))
        ],
      ),
    );
  }
}

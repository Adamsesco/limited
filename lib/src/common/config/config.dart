import 'package:flutter/material.dart';

class Config {
  static Color col_rose = const Color(0xfff15a60);
  static Color col_green = const Color(0xff1bb744);
  static Color col_grey_white = const Color(0xfffafafa);
  static Color col_grey_dark =  Color(0xff60687e);
  static Color col_grey_medium = const Color(0xffafb3be);
  static Color col_red = const Color(0xfff00342);
  static Color col_blue = const Color(0xff3e67d6);
  static Color col_unselected = const Color(0xffCCCCCC);

  static Color col_g = const Color(0xffba395b);
  static Color col_grey_d = const Color(0xff3e3e3e);
  static Color col_grey_bordure = const Color(0xff707070);


}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/config/config.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/services/storage_util.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RestService {
  final JsonDecoder _decoder = new JsonDecoder();

  put(url2, data) async {
    var con = true;

    var results;
    if (con) {
      //data = JSON.encode(data);
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.put(url1, body: data);
      String jsonBody = response.body;

      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = jsonDecode(jsonBody);
        results = postsContainer;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

  get(url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));



      var response = await http.get(url1);

      print(url1);

      String jsonBody = response.body;
      print(response.body);
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        results = _decoder.convert(jsonBody);
      }
    } else {
      results = "No Internet";
    }
    return results;
  }

//headers: {"content-type": "application/json"},
  post(url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      print(response.body);
      var statusCode = response.statusCode;
      /* if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {*/
      var postsContainer = _decoder.convert(jsonBody);
      results = postsContainer;
    } else {
      results = "No Internet";
    }
    return results;
  }



  post2(url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull("http://134.122.118.107/limited/" + url2));

      print(url1);
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      /* if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {*/
      var postsContainer = _decoder.convert(jsonBody);
      results = postsContainer;
    } else {
      results = "No Internet";
    }
    return results;
  }

  post_register(url2, data) async {
    var con = true;
    int statusCode;

    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      print(jsonBody);
/*
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("id", json.decode(jsonBody)["results"]["id"]);*/

      statusCode = json.decode(jsonBody)["status"];
    } else {
      statusCode = 190;
    }
    return statusCode;
  }








  get_fav(item, String user) async {

    print({"product_id": "$item", "token": "$user"});

    var js = json.encode({"product_id": "$item", "token": "$user"});
    var a = await post("addtoSaveCart", js);
    return a;
  }

  /*check_fav(item, user) async {
    var js = {"item_id": "$item", "user_id": "$user"};
    var a = await get("users/fav/check?item_id=$item&user_id=$user");
    return a;

  }*/

  post_login_new(url2, func, context, data) async {
    var con = true;
    int statusCode;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;

      statusCode = json.decode(jsonBody)["status"];

      /* if(json.decode(jsonBody)["status"] == 200)
      {

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("id", json.decode(jsonBody)["data"]["id"]);
        prefs.setString("login_token", json.decode(jsonBody)["data"]["login_token"]);

        print(json.decode(jsonBody)["data"]["id"]);

        if(func.toString() != "null")
        {
          func(json.decode(jsonBody)["data"]["id"]);
        }
        Navigator.pop(context);


      }*/
      if (statusCode == 400 && json.decode(jsonBody)["code"] == 2) {
        statusCode = 201;
      } else if (statusCode == 400 && json.decode(jsonBody)["code"] == 3) {
        statusCode = 209;
      }
    } else {
      statusCode = 190;
    }
    return statusCode;
  }

  post_login(url2, context, data) async {
    var con = true;
    int statusCode;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.post(url1, body: data);
      String jsonBody = response.body;
      statusCode = json.decode(jsonBody)["status"];

      if (json.decode(jsonBody)["status"] == 200) {

        StorageUtil.inicializar().then((_) async {
          var myProvider = Provider.of<LoginProvider>(context, listen: false);
          myProvider.mitexto = 'Provider';
          myProvider.isAuthenticate = true;
          StorageUtil.putBool('isAutenticado', true);

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString("id", json.decode(jsonBody)["results"]["id"]);
          prefs.setString("avatar", json.decode(jsonBody)["results"]["avatar"]);
          prefs.setString("username", json.decode(jsonBody)["results"]["username"]);

          prefs.setString(
              "login_token", json.decode(jsonBody)["results"]["token"]);
          prefs.setString("mobile", json.decode(jsonBody)["results"]["phone"]);
          prefs.setString("email", json.decode(jsonBody)["results"]["email"]);

          myProvider.user = User(prefs.getString("id"), prefs.getString("username"), prefs.getString("avatar"), "", DateTime.now(), prefs.getString("mobile"), prefs.getString("username"));
          Navigator.pop(context);
        });
      } else if (json.decode(jsonBody)["status"] == 400) {
        statusCode = 400;
      }
    } else {
      statusCode = 190;
    }
    return statusCode;
  }

  delete(url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(ConfigApp.base_url + url2));
      var response = await http.delete(url1);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer.length == 0;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }
}

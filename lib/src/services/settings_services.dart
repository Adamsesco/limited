import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings_Services {
  RestService rest = new RestService();


  user_informations(id) async {
    var pr_resp = await rest.get('users/' + id);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp);
  }


  user_info() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String id = prefs.getString("id");
    var pr_resp = await rest.get('users/' + id);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    print(pr_resp);
    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp);
  }
}

import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/services/rest_services.dart';

class CategoriesService {


  RestService rest = new RestService();

  get_all_categories() async {
    var pr_resp = await rest.get('categories');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    return sp
        .map((var contactRaw) => new Category.fromMap(contactRaw))
        .toList();
  }

  get_sub_categories(cat_id) async {
    var pr_resp = await rest.get('subcategories/' + cat_id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp;
    if (sp == null)
      return [];
    else
      return sp
          .map((var contactRaw) => new Category.fromMap(contactRaw))
          .toList();
  }

//http://washy-car.com/pets/api/categories/{cat_id}

}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';

class InboxItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w800);
    TextStyle text2 = TextStyle(
        color: Config.col_unselected,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w500);
    TextStyle text11 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(10),
        fontWeight: FontWeight.w500);
    return  Row(
      children: [
        SizedBox(
            height: ScreenUtil().setWidth(62),
            width: ScreenUtil().setWidth(62),
            child: Stack(
              children: <Widget>[
                CircleAvatar(
                  radius: ScreenUtil().setWidth(58.5),
                  child: ClipOval(
                    child: Image.asset(
                      'assets/images/profile.jpg',
                      height: ScreenUtil().setWidth(87),
                      width: ScreenUtil().setWidth(87),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                    top: 1,
                    left: 0,
                    child: Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                      padding: EdgeInsets.all(2.5),
                      child: CircleAvatar(
                        radius: 8,
                        backgroundColor: Config.col_blue,
                      ),
                    )),
              ],
            )),
        Container(
          width: ScreenUtil().setWidth(16),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                width: ScreenUtil().setWidth(270),
                child: Row(
                  children: [
                    Text(
                      "Adam DAAIF",
                      style: text1,
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      "12:20pm",
                      style: text2,
                    )
                  ],
                )),
            Container(
              height: ScreenUtil().setHeight(12),
            ),
            Container(
                width: ScreenUtil().setWidth(260),
                child: Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt…",
                  style: text11,
                  maxLines: 2,
                ))
          ],
        )
      ],
    );
  }
}

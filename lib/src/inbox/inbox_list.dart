import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/inbox/inbox_item.dart';

class InBoxList extends StatefulWidget {
  @override
  _InBoxListState createState() => _InBoxListState();
}

class _InBoxListState extends State<InBoxList> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(29),
            top: ScreenUtil().setHeight(16),),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(29),

            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(11),
              fontWeight: FontWeight.w500),
        ));

    Widget divid = Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(12),
        bottom: ScreenUtil().setHeight(12),
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );

    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      text("Inbox"),
      Container(
        height: 2,
      ),
      text2("3 new messages"),
      Expanded(
          child: ListView(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(12), right: ScreenUtil().setWidth(12)),
        children: [
          InboxItem(),
          divid,
          InboxItem(),
          divid,
          InboxItem(),
          divid,
          InboxItem(),
          divid,
          InboxItem(),
        ],
      ))
    ]);
  }
}

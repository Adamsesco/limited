import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/dashboard.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/items/product_item2.dart';
import 'package:limited/src/services/products_services.dart';

class MyMarket extends StatefulWidget {
  @override
  _MyMarketState createState() => _MyMarketState();
}

class _MyMarketState extends State<MyMarket> {
  ProductSServices prods = new ProductSServices();

  Dashboard1 dashboard1 = new Dashboard1();
  List<Product> products = new List<Product>();
  bool load= true;

  get_maarket_details() async {
    var a = await prods.getMarket();

    print(a);
    if (!this.mounted) return;

    setState(() {
      dashboard1=a;
      load=false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_maarket_details();
  }
  ///  "totalPayments": 0, "activeItems": 3, "unactiveItems": 0, "soldedItems": 0

  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: Config.col_green,
        fontSize: ScreenUtil().setSp(15),
        fontWeight: FontWeight.w800);

    TextStyle text1_btn = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w700);
    TextStyle text2 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(10),
        fontWeight: FontWeight.w400);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    TextStyle text3 = TextStyle(
        color: Config.col_grey_dark,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);

    Widget wid2 = Column(
      children: [
        Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(38),
              color: Color(0xffF7F6F9),
            ),
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 6),
                child: Column(
                  children: [
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Color(0xffF7F6F9),
                        ),
                        child: Padding(
                            padding: const EdgeInsets.all(12),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "08-03-2021  23:19:45",
                                      style: TextStyle(
                                        color: Config.col_grey_dark,
                                        fontSize: ScreenUtil().setSp(10),
                                      ),
                                    ),
                                    Text(
                                      "Sarah Micheals",
                                      style: TextStyle(
                                          color: Config.col_grey_d,
                                          fontSize: ScreenUtil().setSp(11),
                                          fontWeight: FontWeight.w600),
                                    )
                                  ],
                                ),
                                Spacer(),
                                Container(
                                  width: ScreenUtil().setWidth(16),
                                ),
                                Container(
                                    height: ScreenUtil().setHeight(32),
                                    child: RaisedButton(
                                      color: Config.col_green,
                                      elevation: 1,
                                      shape: new RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(28),
                                        ),
                                      ),
                                      onPressed: () {},
                                      child: Text(
                                        "Withdraw",
                                        style: text1_btn,
                                      ),
                                    )),
                              ],
                            )))
                  ],
                )))
      ],
    );

    TextStyle text4 = TextStyle(
        color: Config.col_grey_medium,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);
    Widget item(String txt1, String txt2) => Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              txt1.toString(),
              style: text3,
            ),
            Container(
              height: ScreenUtil().setHeight(4),
            ),
            Text(
              txt2,
              style: text4,
            ),
          ],
        );

    Widget dashboard = Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Color(0xffF7F6F9),
        ),
        width: MediaQuery.of(context).size.width * 0.94,
        child: Padding(
            padding: const EdgeInsets.all(12),
            child: Column(children: [
              Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Total earnings in ${DateTime.now().month}",
                            style: text3,
                          ),
                          Text(
                            "${dashboard1.totalPayments} KD",
                            style: text1,
                          )
                        ],
                      ),
                      Spacer(),
                      SvgPicture.asset("assets/images/icons/calendar.svg")
                    ],
                  )),
              Container(
                height: ScreenUtil().setHeight(19),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  item(dashboard1.activeItems, "ACTIVE"),
                  Container(
                    width: ScreenUtil().setWidth(58),
                  ),
                  item(dashboard1.soldedItems, "SOLD"),
                  Container(
                    width: ScreenUtil().setWidth(58),
                  ),
                  item(dashboard1.unactiveItems, "UNSOLD"),
                ],
              ),
              Container(
                height: ScreenUtil().setHeight(20),
              ),
            ])));

    return  load == true
        ? Center(
      child: Scaffold(body: CupertinoActivityIndicator(),)
    )
        : DefaultTabController(
        length: 4,
        child: Scaffold(
            appBar: LAppBar(
              false,
              leading_widget: IconButton(
                icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              actions_widget: IconButton(
                icon: Container(
                    height: ScreenUtil().setWidth(24),
                    width: ScreenUtil().setWidth(24),
                    child: SvgPicture.asset(
                      /*_index==4?"assets/images/icons/edit.svg":*/
                      "assets/images/icons/settings.svg",
                      height: ScreenUtil().setWidth(24),
                      width: ScreenUtil().setWidth(24),
                      fit: BoxFit.cover,
                    )),
                padding: EdgeInsets.all(2),
                onPressed: () {},
              ),
            ),
            body: Padding(
              padding: EdgeInsets.only(
                  left: ScreenUtil().setWidth(10),
                  right: ScreenUtil().setWidth(6)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: ScreenUtil().setHeight(10),
                  ),
                  dashboard,
                  Container(
                    height: ScreenUtil().setHeight(8),
                  ),
                  wid2,
                  text("Market"),
                  Container(
                    height: ScreenUtil().setHeight(16),
                  ),
                  Container(
                      // padding: EdgeInsets.only(left: ScreenUtil().setWidth(29)),
                      child: ButtonsTabBar(
                    backgroundColor: Config.col_grey_d,
                    radius: 24,
                    unselectedBackgroundColor: const Color(0xffF1F0F5),
                    unselectedLabelStyle: TextStyle(color: Colors.black),
                    labelStyle: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    tabs: [
                      Tab(
                        text: "ACTIVE",
                      ),
                      Tab(
                        text: "INACTIVE",
                      ),
                      Tab(
                        text: "SOLD",
                      ),
                      Tab(
                        text: "UNSOLD",
                      ),
                    ],
                  )),
                  Expanded(
                      child: DefaultTabController(
                    length: 4,
                    child: Expanded(
                        child: TabBarView(children: [

                   ListView(
                    children: dashboard1.products
                        .map((e) => ProductItem2(e))
                        .toList(),
                  ),
                  ListView(
                    children: dashboard1.products
                        .map((e) => ProductItem2(e))
                        .toList(),
                  ),
                  ListView(
                    children: dashboard1.products
                        .map((e) => ProductItem2(e))
                        .toList(),
                  ),
                  ListView(
                    children: dashboard1.products
                        .map((e) => ProductItem2(e))
                        .toList(),
                  ),
                    ])),
                  ))
                ],
              ),
            )));
  }
}

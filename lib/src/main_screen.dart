import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/common/widgets/verify_login.dart';
import 'package:limited/src/messenger/chats.dart';
import 'package:limited/src/profile/edit_profile.dart';
import 'package:limited/src/publish/publish3.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/common/widgets/floating_navbar.dart';
import 'package:limited/src/home/Featured_items/saved_items.dart';
import 'package:limited/src/home/categories/categories_list.dart';
import 'package:limited/src/home/home_screen.dart';
import 'package:limited/src/inbox/inbox_list.dart';
import 'package:limited/src/notifications/notifications_list.dart';
import 'package:limited/src/profile/profile_screen.dart';
import 'package:limited/src/publish/publish1.dart';
import 'package:limited/src/publish/publish3.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/settings/settings_list.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _index = 0;
  PageController _pageController = new PageController();
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  go_to_message() {
    setState(() => _index = 3);

    _pageController.animateToPage(
      3,
      duration: Duration(milliseconds: 200),
      curve: Curves.linear,
    );
  }

  onTap(index) {
    setState(() => _index = index);
    _pageController.animateToPage(
      index,
      duration: Duration(milliseconds: 200),
      curve: Curves.linear,
    );
  }

  online_on() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id");

    if (id != null) {
      FirebaseDatabase.instance
          .reference()
          .child("status")
          .child(id)
          .update({"online": true});

      FirebaseDatabase.instance
          .reference()
          .child("status")
          .child(id)
          .onDisconnect()
          .update({"online": false});
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    online_on();
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
      key: scaffoldKey,
        //backgroundColor: Colors.grey,
        appBar: LAppBar(
          _index == 4 ? false : true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/notif.svg"),
            onPressed: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return NotificationsList();
              }));
            },
          ),
          actions_widget: myProvider.isAuthenticate == false
              ? Container()
              : IconButton(
                  icon: SvgPicture.asset("assets/images/icons/edit.svg"),
                  onPressed: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return EditProfilee(myProvider.user);
                    }));
                  },
                ),
        ),
        //If you want to show body behind the navbar, it should be true
        extendBody: true,
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: [
            HomeScreen(),
            myProvider.isAuthenticate == false
                ? Container(child: NotLogin())
                : SavedItems(),
            myProvider.isAuthenticate == false
                ? Container(child: NotLogin())
                : Publish1(),
            // SettingsList(),
            myProvider.isAuthenticate == false
                ? Container(child: NotLogin())
                : Chats(),
            myProvider.isAuthenticate == false
                ? Container(child: NotLogin())
                : ProfileScreen(go_to_message,scaffoldKey),
          ],
        ),
        bottomNavigationBar: Container(
          child: FloatingNavbar(
            padding: EdgeInsets.only(top: 0, bottom: 0),
            onTap: (int val) {
              setState(() => _index = val);
              _pageController.animateToPage(
                val,
                duration: Duration(milliseconds: 200),
                curve: Curves.linear,
              );
            },
            currentIndex: _index,
            backgroundColor: Colors.white,
            selectedItemColor: Config.col_blue,
            unselectedItemColor: Config.col_unselected,
            borderRadius: 100,
            items: [
              FloatingNavbarItem(icon: "assets/images/icons/home.svg"),
              FloatingNavbarItem(icon: "assets/images/icons/like.svg"),
              FloatingNavbarItem(
                  icon: "assets/images/icons/plus.svg", ismiddle: true),
              FloatingNavbarItem(icon: "assets/images/icons/msg.svg"),
              FloatingNavbarItem(
                  icon: myProvider.isAuthenticate == false
                      ? "assets/images/icons/logo.svg"
                      : myProvider.user.image),
            ],
          ),
        ));
  }
}

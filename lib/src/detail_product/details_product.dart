import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;

import 'package:audioplayers/audio_cache.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

//import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/bids/bid_page.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/common/widgets/dotted_slideer.dart';
import 'package:limited/src/common/widgets/favorite_widget.dart';
import 'package:limited/src/common/widgets/gallery.dart';
import 'package:limited/src/detail_product/custombar.dart';
import 'package:limited/src/home/Featured_items/featured_items.dart';
import 'package:limited/src/messenger/conversation.dart';
import 'package:limited/src/otp/otp_verification.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/signin/signin.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailsProduct extends StatefulWidget {
  DetailsProduct(this.product);

  Product product;

  @override
  _DetailsProductState createState() => _DetailsProductState();
}

class _DetailsProductState extends State<DetailsProduct>
    with TickerProviderStateMixin {
  RestService rest = new RestService();
  StreamSubscription _onDestroy;
  StreamSubscription<String> _onUrlChanged;
  bool lod = false;
  String track_id = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var price = 0.0;
  var _value = 0.0;
  User user_last_bid;
  bool start = false;
  AnimationController controller;

  load() {
    setState(() {
      lod = false;
    });
  }

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  Future<AudioPlayer> playLocalAsset() async {
    AudioCache cache = new AudioCache();
    return await cache.play("sound.mpeg");
  }

  get_price() {
    FirebaseDatabase.instance
        .reference()
        .child("last_price")
        .child(widget.product.id)
        .onValue
        .listen((val) {
      if (!this.mounted) return;
      var d;
      // try {

      if (val.snapshot.value.toString() == "null") {
        setState(() {
          price = double.parse(widget.product.last_price);
        });
      } else {
        if (this.mounted) {
          setState(() {
            controller = AnimationController(
              vsync: this,
              duration: Duration(seconds: 5),
            );

            controller.reverse(
                from: controller.value == 0.0 ? 1.0 : controller.value);
            if (start == true) {
              playLocalAsset();
            }

            setState(() {
              start = true;
            });
            d = val.snapshot.value;
            price = val.snapshot.value["price"];
            _value = price;

            user_last_bid = new User(
                val.snapshot.value["id"],
                val.snapshot.value["name"],
                val.snapshot.value["avatar"],
                "",
                DateTime.now(),
                "",
                val.snapshot.value["name"]);

            /// _startprice.text = _value.toString();
          });
        }
      }
      /*  } catch (e) {}*/
    });
  }

  ccc() async {
    var b = await rest.get("checkPayment/" + track_id.toString());
    if (b["ispaied"].toString() == "1") {
      Alert(
        context: context,
        type: AlertType.success,
        title: "Payment with succeess",
        desc: "Successfully added !",
        buttons: [
          DialogButton(
            color: Config.col_blue,
            child: Text(
              "Return",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.pop(context);
              //  Navigator.pop(context);
              /// widget.end_func();
              //widget.func(1);
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
          backgroundColor:
              b["ispaied"].toString() == "1" ? Colors.green : Colors.red[800],
          content: new Text(
            b["ispaied"].toString() == "1" ? "Success" : "Payment failed",
            style: TextStyle(color: Colors.white, fontSize: 16),
          )));
    }
  }

  ProductSServices prod = new ProductSServices();

/*
  get_details() async {
    var a = await prod.get_product_deetails(widget.product.id);
    if (!this.mounted) return;
    setState(() {
      widget.product.user_id = a.user_id;
      widget.product.avatar = a.avatar;
    });
  }
*/
  String id = "";

  getuserid() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!this.mounted) return;

    setState(() {
      id = prefs.getString("id");
    });
  }

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return OtpVerification();
              }));
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  Duration remaining = DateTime.now().difference(DateTime.now());
  Timer t;
  int days = 0, hrs = 0, mins = 0;

  startTimer() async {
    t = Timer.periodic(Duration(seconds: 1), (timer) {
      print("-------dddd");
      print(widget.product.expired_date);
      if (!this.mounted) return;
      setState(() {
        remaining =
            widget.product.expired_date.difference(DateTime.now().toUtc());
        mins = remaining.inMinutes;
        hrs = mins >= 60 ? mins ~/ 60 : 0;
        days = hrs >= 24 ? hrs ~/ 24 : 0;
        hrs = hrs % 24;
        mins = mins % 60;
      });
    });
  }

  @override
  void initState() {
    super.initState();

    getuserid();
    /* controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    controller.reverse(from: controller.value == 0.0 ? 1.0 : controller.value);*/

    if (widget.product.type.toString() == "1") {
      get_price();
    }
    if (widget.product.type.toString() == "1" &&
        widget.product.expired_date != null) {
      if (widget.product.expired_date.isAfter(DateTime.now().toUtc())) {
        startTimer();
      }
    }
  }

  counter_wid() => Container(
        width: 32.w,
        height: 32.w,
        child: AnimatedBuilder(
            animation: controller,
            builder: (BuildContext context, Widget child) {
              return controller.value.toString() == "0.0"
                  ? Container()
                  : CustomPaint(
                      painter: CustomTimerPainter(
                      animation: controller,
                      color: Colors.white,
                      backgroundColor: Colors.grey[700].withOpacity(0.62),
                    ));
            }),
      );

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (controller != null) controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1_btn(color) => TextStyle(
        color: color,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);
    TextStyle text3 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);

    var myProvider = Provider.of<LoginProvider>(context);

    return Scaffold(
        key: _scaffoldKey,
        extendBodyBehindAppBar: true,
        body: ListView(children: [
          Stack(
            children: [
              DottedSlider(
                  color: Colors.white,
                  maxHeight: ScreenUtil().setHeight(428),
                  children: widget.product.image
                      .map<Widget>((String im) => InkWell(
                            child: FadeInImage.assetNetwork(
                              image: im,
                              placeholder: "assets/images/placeholder.png",
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil().setHeight(428),
                              fit: BoxFit.cover,
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        GalleryPhotoViewWrapper(
                                      galleryItems: widget.product.images,
                                      backgroundDecoration: const BoxDecoration(
                                        color: Colors.black,
                                      ),
                                      initialIndex:
                                          widget.product.images.indexOf(im),
                                    ),
                                  ));
                            },
                          ))
                      .toList()),
              Positioned(
                  bottom: ScreenUtil().setHeight(42),
                  left: ScreenUtil().setWidth(27),
                  child: Container(
                    child: user_last_bid == null
                        ? SizedBox()
                        : Container(
                            width: 321.59.w,
                            padding: EdgeInsets.only(
                                left: 10.w, right: 10.w, top: 6.h, bottom: 6.h),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(32.0)),
                                color: Colors.grey[700].withOpacity(0.62)),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  child: ClipOval(
                                    child: Image.network(
                                      user_last_bid.image,
                                      height: ScreenUtil().setWidth(43),
                                      width: ScreenUtil().setWidth(43),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 12.w,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      user_last_bid.userename.toString(),
                                      style: TextStyle(
                                        fontSize: 16.sp,
                                        color: Colors.white,
                                        fontFamily: "Roboto Condensed",
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Container(
                                      height: 4.h,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Placed a bid of ",
                                          style: TextStyle(
                                              fontFamily: "Roboto light",
                                              fontSize: 14.sp,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                          width: 2.w,
                                        ),
                                        Text(
                                          (price.toString() + " KD"),
                                          style: TextStyle(
                                            fontSize: 14.sp,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                Stack(children: [
                                  counter_wid()

                                  /* Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      timerString,
                                      style: TextStyle(
                                          fontSize: 12.0, color: Colors.white),
                                    ),
                                  )*/
                                ])
                              ],
                            )),
                  )),
              Positioned(
                  top: ScreenUtil().setHeight(18),
                  left: ScreenUtil().setWidth(12),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/arrow_back.svg",
                            height: 15,
                            color: Colors.white,
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/down.svg",
                            color: Colors.white,
                          ),
                        ),
                        FavoriteButton(widget.product)
                        /*IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/like.svg",
                            color: Colors.white,
                          ),
                        )*/
                        ,
                        Container(
                          width: ScreenUtil().setWidth(24),
                        )
                      ],
                    ),
                  ))
            ],
          ),
          Container(
              padding: EdgeInsets.only(
                  left: ScreenUtil().setWidth(16),
                  right: ScreenUtil().setWidth(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: ScreenUtil().setHeight(16),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        widget.product.name,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.2,
                          fontFamily: "Roboto Condensed",
                          fontWeight: FontWeight.w600,
                          fontSize: ScreenUtil().setSp(16.5),
                        ),
                      )),
                  Container(
                    height: ScreenUtil().setHeight(4),
                  ),
                  Container(
                      height: ScreenUtil().setHeight(40),
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        widget.product.description.toString(),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.4,
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(14.5),
                        ),
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/images/icons/adr.svg",
                              width: 12,
                            ),
                            Container(
                              width: 5,
                            ),
                            Text(
                              widget.product.city.toString() == "null"
                                  ? "--"
                                  : widget.product.city,
                              style: text3,
                            )
                          ],
                        ),
                        Spacer(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              widget.product.type.toString() == "0"
                                  ? (widget.product.last_price.toString() +
                                      " KD")
                                  : (price.toString() + " KD"),
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: ScreenUtil().setSp(15.5),
                              ),
                            ),
                            Container(
                              height: 4,
                            ),
                            widget.product.type.toString() == "0"
                                ? Text(
                                    widget.product.time.toString(),
                                  )
                                : widget.product.expired_date == null
                                    ? widget.product.expired_date
                                            .isAfter(DateTime.now().toUtc())
                                        ? Container()
                                        : Text(
                                            "") /** widget.product.expired_date.toString()*/
                                    : Container(
                                        width: 180.w,
                                        // height: 100,
                                        child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      '$days d ',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12.sp),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Container(
                                                      width: 4.w,
                                                    ),
                                                    Text(
                                                      '$hrs h',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12.sp),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Container(
                                                      width: 4.w,
                                                    ),
                                                    Text(
                                                      '$mins m',
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12.sp),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ]),
                                              /**
                                       *
                                       * Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment
                                          .spaceEvenly,
                                          children: [
                                          Expanded(
                                          child: Padding(
                                          padding:
                                          const EdgeInsets.all(
                                          8.0),
                                          child: Text(
                                          'Days',
                                          style: TextStyle(
                                          color: Colors.black,
                                          fontSize:  12.sp),
                                          textAlign:
                                          TextAlign.center,
                                          ),
                                          ),
                                          ),
                                          Expanded(
                                          child: Padding(
                                          padding:
                                          const EdgeInsets.all(
                                          8.0),
                                          child: Text(
                                          'Hours',
                                          style: TextStyle(
                                          color: Colors.black,
                                          fontSize:  12.sp),
                                          textAlign:
                                          TextAlign.center,
                                          ),
                                          ),
                                          ),
                                          Expanded(
                                          child: Padding(
                                          padding:
                                          const EdgeInsets.all(
                                          8.0),
                                          child: Text(
                                          'Minutes',
                                          style: TextStyle(
                                          color: Colors.black,
                                          fontSize:  12.sp),
                                          textAlign:
                                          TextAlign.center,
                                          ),
                                          ),
                                          ),
                                          ])
                                       */
                                            ])),
                          ],
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(40),
                      bottom: ScreenUtil().setHeight(29),
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          id != widget.product.user_id
                              ? widget.product.type.toString() == "0"
                                  ? lod == true
                                      ? Container(
                                          padding: EdgeInsets.only(left: 24.w),
                                          child: CupertinoActivityIndicator())
                                      : Container(
                                          height: ScreenUtil().setHeight(44),
                                          child: RaisedButton(
                                            color: Config.col_blue,
                                            elevation: 1,
                                            shape: new RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(28),
                                              ),
                                            ),
                                            onPressed: () async {
                                              SharedPreferences prefs =
                                                  await SharedPreferences
                                                      .getInstance();
                                              String id = prefs.getString("id");

                                              if (myProvider.isAuthenticate ==
                                                  false)
                                                please_login();
                                              else {
                                                setState(() {
                                                  lod = true;
                                                });

                                                var b = await rest.post2(
                                                    "makePayment",
                                                    json.encode({
                                                      "id": widget.product.id,
                                                      "price": widget
                                                          .product.last_price
                                                          .toString(),
                                                      "type":
                                                          widget.product.type,
                                                      "userid": id
                                                    }));

                                                print(b);
                                                print(b["link"]);

                                                track_id = b["track"];

                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) => Scaffold(
                                                          appBar: new AppBar(
                                                              backgroundColor: Config.col_blue,
                                                              leading: IconButton(
                                                                icon: Icon(Icons
                                                                    .close),
                                                                onPressed: () {
                                                                  Navigator.pop(
                                                                      context);
                                                                  ccc();
                                                                  load();
                                                                },
                                                              ),
                                                              title: Text(
                                                                "",
                                                              )),
                                                          body: WebView(
                                                            initialUrl:
                                                                b["link"],
                                                            javascriptMode:
                                                                JavascriptMode
                                                                    .unrestricted,
                                                            onPageFinished:
                                                                (value) {
                                                              setState(() {
                                                                print(
                                                                    "====your page is load");
                                                              });
                                                            },
                                                          )) /*WebviewScaffold(
                          url: b["link"],
                          appBar: new AppBar(
                            title: new Text(""),
                          ))*/
                                                      ,
                                                    ));

                                                /* */

                                                /*ChromeSafariBrowser browser =
                                            new MyChromeSafariBrowser(
                                                new InAppBrowser(), ccc, load);
                                        browser.open(
                                          b["link"],
                                          options: {
                                            "addShareButton": false,
                                            "toolbarBackgroundColor": "#FFFFFF",
                                            "dismissButtonStyle": 1,
                                            "preferredBarTintColor": "#FFFFFF",
                                          },
                                        );
                                      }*/
                                              }
                                            },
                                            child: Text(
                                              "BUY NOW",
                                              style: text1_btn(Colors.white),
                                            ),
                                          ))
                                  : Container()
                              : Container(),
                          SizedBox(
                            width: ScreenUtil().setWidth(10),
                          ),
                          widget.product.expired_date != null?  widget.product.expired_date
                                  .isAfter(DateTime.now().toUtc())
                              ? id != widget.product.user_id
                                  ? widget.product.type.toString() == "1"
                                      ? Container(
                                          height: ScreenUtil().setHeight(44),
                                          child: RaisedButton(
                                            color: Colors.white,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(28.0),
                                                side: BorderSide(
                                                    color: Config.col_grey_d)),
                                            onPressed: () async {
                                              if (myProvider.isAuthenticate ==
                                                  false)
                                                please_login();
                                              else {
                                                SharedPreferences prefs =
                                                    await SharedPreferences
                                                        .getInstance();
                                                String id =
                                                    prefs.getString("id");

                                                User user_me = new User(
                                                    id,
                                                    prefs.getString("name"),
                                                    prefs.getString("avatar"),
                                                    "",
                                                    DateTime.now(),
                                                    "",
                                                    prefs
                                                        .getString("username"));
                                                Navigator.push(context,
                                                    new MaterialPageRoute(
                                                        builder: (BuildContext
                                                            context) {
                                                  return BidPage(
                                                      widget.product.id
                                                          .toString(),
                                                      widget.product.last_price,
                                                      user_me);
                                                }));
                                              }
                                            },
                                            child: Text(
                                              "PLACE A BID",
                                              style:
                                                  text1_btn(Config.col_grey_d),
                                            ),
                                          ))
                                      : Container()
                                  : Container()
                              : Container(
                                  padding: EdgeInsets.only(
                                      left: 10.w,
                                      right: 10.w,
                                      top: 12.h,
                                      bottom: 12.h),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0)),
                                      color:
                                          Colors.red.withOpacity(0.10)),
                                  child: Text(
                                    "The auction is done ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.red[800]),
                                  )): Container(),
                          SizedBox(
                            width: ScreenUtil().setWidth(20),
                          ),
                          id != widget.product.user_id
                              ? Container(
                                  height: ScreenUtil().setHeight(44),
                                  child: RaisedButton(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(28.0),
                                        side: BorderSide(
                                            color: Config.col_grey_d)),
                                    onPressed: () async {
                                      String idpost = widget.product.id;
                                      if (myProvider.isAuthenticate == false)
                                        please_login();
                                      else {
                                        SharedPreferences prefs =
                                            await SharedPreferences
                                                .getInstance();
                                        String id = prefs.getString("id");

                                        User user_me = new User(
                                            id,
                                            prefs.getString("avatar"),
                                            "",
                                            "",
                                            DateTime.now(),
                                            "",
                                            prefs.getString("username"));
                                        User user = new User(
                                            widget.product.user_id,
                                            widget.product.username,
                                            widget.product.avatar
                                            /*"https://res.cloudinary.com/dgxctjlpx/image/upload/v1616440356/Composant_1_1YYY_yxqhde.png"*/,
                                            "",
                                            DateTime.now(),
                                            "",
                                            widget.product.username);
                                        Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                            builder: (BuildContext context) {
                                              return Conversation(
                                                user_me,
                                                user,
                                                idpost,
                                                post: widget.product,
                                              );
                                            },
                                          ),
                                        );
                                      }
                                    },
                                    child: Text(
                                      "MESSAGE",
                                      style: text1_btn(Config.col_grey_d),
                                    ),
                                  ))
                              : Container(),
                        ]),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        "Item description",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.2,
                          fontFamily: "Roboto Condensed",
                          fontWeight: FontWeight.w600,
                          fontSize: ScreenUtil().setSp(16.5),
                        ),
                      )),
                  Container(
                    height: ScreenUtil().setHeight(19),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      widget.product?.l_desc.toString() == "null"
                          ? ""
                          : widget.product?.l_desc.toString(),
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    ),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(19),
                  ),
                  Container(
                    height: ScreenUtil().setHeight(19),
                  ),
                  FeaturedItems([])
                ],
              )),
        ]));
  }
}
/*
class MyChromeSafariBrowser extends ChromeSafariBrowser {
  MyChromeSafariBrowser(browserFallback, this.ccc, this.load)
      : super(browserFallback);

  var ccc;
  var load;

  @override
  void onOpened() {
    load();
  }

  @override
  void onLoaded() {
    print("ChromeSafari browser loaded");
    load();
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
    ccc();
  }
}

*/

class CustomTimerPainter extends CustomPainter {
  CustomTimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 3.0
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(CustomTimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';


class CustomAppBar extends PreferredSize {
  final double rating;

  CustomAppBar({@required this.rating});

  @override
  // AppBar().preferredSize.height provide us the height that appy on our app bar
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Color(0x44000000),
        padding:
        EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
        child: Row(
          children: [
            SizedBox(
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(60),
                ),
                color: Colors.white,
                padding: EdgeInsets.zero,
                onPressed: () => Navigator.pop(context),
                child: SvgPicture.asset(
                  "assets/images/icons/arrow_back.svg",
                  height: 15,
                  color: Colors.blue,
                ),
              ),
            ),
            Spacer(),

          ],
        ),
      ),
    );
  }
}

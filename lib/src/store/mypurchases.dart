import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/items/product_item2.dart';
import 'package:limited/src/services/products_services.dart';


class MyPurchases extends StatefulWidget {

  @override
  _MyPurchasesState createState() => _MyPurchasesState();
}

class _MyPurchasesState extends State<MyPurchases> {

  bool load = true;
  ProductSServices prods = new ProductSServices();

  List<Product> products = new List<Product>();

  get_my_purchases() async {
    var a = await prods.get_purchases();
    if (!this.mounted) return;
    setState(() {
      products = List<Product>.from(a);
      load = false;
    });
  }

  @override
  void initState() {
    super.initState();
    get_my_purchases();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4(bool check) => TextStyle(
        color: check ? Config.col_blue : Config.col_grey_d,
        fontSize: ScreenUtil().setSp(check ? 16 : 14),
        fontWeight: check ? FontWeight.w900 : FontWeight.w500);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w500),
        ));

    return Scaffold(
        appBar: LAppBar(
          true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          text("My purchases"),
          Container(
            height: 2,
          ),
          Row(
            children: [
              Container(
                  width: ScreenUtil().setWidth(310),
                  child: text2(
                      "Here you can find the purchases you have ."))
            ],
          ),

          //ProductItem2(Product.),

          load == true
              ? Center(
            child: CupertinoActivityIndicator(),
          )
              : products.isEmpty
              ? Center(
            child: Text("No result found !"),
          )
              : Expanded(
              child: ListView(
                  children: products
                      .map((e) => ProductItem2(e))
                      .toList()) /* Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(38),
                    color: Color(0xffF7F6F9),
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: Color(0xffF7F6F9),
                                  ),
                                  width:
                                      MediaQuery.of(context).size.width * 0.94,
                                  child: Padding(
                                      padding: const EdgeInsets.all(12),
                                      child: Row(
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "08-03-2021  23:19:45",
                                                style: TextStyle(
                                                  color: Config.col_grey_dark,
                                                  fontSize:
                                                      ScreenUtil().setSp(10),
                                                ),
                                              ),
                                              Text(
                                                "AUCTIONS start",
                                                style: TextStyle(
                                                    color: Config.col_grey_d,
                                                    fontSize:
                                                        ScreenUtil().setSp(11),
                                                    fontWeight:
                                                        FontWeight.w600),
                                              )
                                            ],
                                          ),
                                          Spacer(),
                                          Text(
                                            "900.000 KD",
                                            style: TextStyle(
                                                color: Config.col_blue,
                                                fontSize:
                                                    ScreenUtil().setSp(13),
                                                fontWeight: FontWeight.w600),
                                          )
                                        ],
                                      )))
                            ],
                          )
                        ],
                      )))
            ],
          )*/
          ),
          Container(
            height: ScreenUtil().setHeight(20),
          ),
          // BidItemhist(),
        ]));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/detail_product/details_product.dart';


class ProductStoreItem extends StatelessWidget {
  ProductStoreItem(this.product);

  Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setHeight(198),
            width: ScreenUtil().setWidth(130),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  //<--clipping image

                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                AssetImage("assets/images/placeholder.png"),
                                fit: BoxFit.contain)),
                        child: product.image == null
                            ? Container()
                            : Hero(
                            tag: product.image,
                            child: FadeInImage.assetNetwork(
                              image: product.image[0],
                              placeholder: "assets/images/placeholder.png",
                              width: ScreenUtil().setWidth(166),
                              height: ScreenUtil().setHeight(156),
                              fit: BoxFit.cover,
                            )))),

                SizedBox(
                  height: 8,
                ),

                Container(
                  // height: ScreenUtil().setHeight(54),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.name.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                SizedBox(
                  height: 4,
                ),
                Container(
                    height: ScreenUtil().setHeight(34),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.description.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                // Expanded(child: Container(),),
                Container(
                  child: Text(
                    product.last_price.toString() +" KD",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(17),
                    ),
                  ),
                ),
              ],
            )),
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
                return DetailsProduct(product);
              }));
        });
  }
}

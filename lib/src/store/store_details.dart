import 'dart:io';

import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/store.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/common/widgets/dotted_slideer.dart';
import 'package:limited/src/common/widgets/toggle_bar.dart';
import 'package:limited/src/home/Featured_items/featured_items.dart';
import 'package:limited/src/services/categories_services.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/store/products_store_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StoreDetails extends StatefulWidget {
  StoreDetails(this.store);

  Store store;

  @override
  _StoreDetailsState createState() => _StoreDetailsState();
}

class _StoreDetailsState extends State<StoreDetails>
    with SingleTickerProviderStateMixin {
  ProductSServices prod = new ProductSServices();
  bool fixed = true;
  TabController _tabController;
  List<Category> cats = new List<Category>();
  CategoriesService cat = new CategoriesService();
  bool load = true;

  getList() async {
    var a = await cat.get_all_categories();
    if (!this.mounted) return;

    setState(() {
      cats = a;
      _tabController = new TabController(length: cats.length, vsync: this);
      load = false;
    });

  }

  get_details() async {
    Store a = await prod.get_store_deetails(widget.store.id.toString());
    if (!this.mounted) return;
    setState(() {

      widget.store.images = a.images;
      widget.store.bids = a.bids;
      widget.store.products = a.products;
    });
  }

  save_images_shop(int im) async {
    /*setState(() {
      uploading1 = true;
      lo = false;
      _isComposing1 = true;
    });*/

    var _imageFile =
    await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    File compressedFile = await FlutterNativeImage.compressImage(
        _imageFile.path,
        quality: 70);

    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + ".jpg");
    await storageReference.put(compressedFile).onComplete.then((val) {
      val.ref.getDownloadURL().then((val) {

        print(val.toString());
        setState(() {
          widget.store.images[im] = val.toString();
        });

        prod.slider_shop(widget.store.images, widget.store.image);
      });
    });
  }

  getid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id");

  }

  @override
  void initState() {
    super.initState();

    getid();
    getList();
    get_details();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: Config.col_blue,
        fontSize: ScreenUtil().setSp(17),
        fontWeight: FontWeight.w800);

    TextStyle text2 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(10),
        fontWeight: FontWeight.w400);

    TextStyle text3 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: LAppBar(true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          )),
      body: ListView(
        children: [
          Container(
            margin: EdgeInsetsDirectional.only(
              top: ScreenUtil().setHeight(17.4).toDouble(),
              end: ScreenUtil().setWidth(17).toDouble(),
              bottom: ScreenUtil().setHeight(8).toDouble(),
              start: ScreenUtil().setWidth(22).toDouble(),
            ),
            child: Row(
              children: <Widget>[
               Container(
                  width: ScreenUtil().setWidth(64).toDouble(),
                  height: ScreenUtil().setWidth(64).toDouble(),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(widget.store.image),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),




                SizedBox(width: ScreenUtil().setWidth(21).toDouble()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.store.name, style: text1),
                    Container(
                      height: 8.h,
                    ),
                    Text(
                      "Member since " +
                          new DateFormat('dd-MM-yyyy')
                              .format(widget.store.created),
                      style: text2,
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(8),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          "assets/images/icons/adr.svg",
                          width: 12,
                        ),
                        Container(
                          width: 5,
                        ),
                        Text(
                          widget.store.addresess == null
                              ? "---"
                              : widget.store.addresess,
                          style: text3,
                        )
                      ],
                    ),
                    Container(
                      height: 6,
                    ),
                    /* if (user.phoneNumber != null &&
                            user.phoneNumber.isNotEmpty)
                          Text(
                            user.phoneNumber,
                            style: TextStyle(
                              fontFamily: 'Avenir LT Std 55 Roman',
                              fontSize: ScreenUtil().setSp(10).toDouble(),
                              letterSpacing: 0.50,
                              color: Color(0xFFC2C4CA),
                            ),
                          ),*/
                  ],
                ),
                Container(
                  width: 12,
                ),
                Expanded(
                  child: Container(),
                ),
                Row(
                  children: [
                    Container(
                        width: ScreenUtil().setWidth(32).toDouble(),
                        child: RawMaterialButton(
                          onPressed: () {
                            /*  final Uri params = Uri(
                                  scheme: 'mailto',
                                  path: '${profile.user.email}',
                                );
                                String url = params.toString();
                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else {
                                  print('Could not launch $url');
                                }*/
                          },
                          elevation: 3.0,
                          //padding: EdgeInsets.all(10.0),
                          shape: CircleBorder(),
                          fillColor: Colors.white,
                          child: SvgPicture.asset(
                            "assets/images/icons/like.svg",
                            color: Colors.black,
                            width: ScreenUtil().setWidth(19).toDouble(),
                            height: ScreenUtil().setWidth(14).toDouble(),
                            fit: BoxFit.cover,
                          ),
                        )),
                  ],
                ),
                Container(
                  width: 12,
                ),
                Container(
                    width: ScreenUtil().setWidth(32).toDouble(),
                    child: RawMaterialButton(
                      onPressed: () {},
                      elevation: 3.0,
                      //padding: EdgeInsets.all(10.0),
                      shape: CircleBorder(),
                      fillColor: Colors.white,
                      child: SvgPicture.asset(
                        "assets/images/icons/down.svg",
                        color: Colors.black,
                        width: ScreenUtil().setWidth(19).toDouble(),
                        height: ScreenUtil().setWidth(14).toDouble(),
                        fit: BoxFit.cover,
                      ),
                    )),
              ],
            ),
          ),
          Container(
            height: 28.h,
          ),
          DottedSlider(
              color: Colors.white,
              maxHeight: ScreenUtil().setHeight(169),
              children: List<String>.from(widget.store.images)
                  .map<Widget>((String im) =>   Stack(children: [

    Positioned.fill( child:Container(
                      padding: EdgeInsets.only(left: 12.w, right: 12.w),
                      child: ClipRRect(
                          //<--clipping image

                          borderRadius: BorderRadius.all(Radius.circular(16)),
                          child: FadeInImage.assetNetwork(
                            image: im,
                            placeholder: "assets/images/placeholder.png",
                            width: MediaQuery.of(context).size.width,
                            height: ScreenUtil().setHeight(169),
                            fit: BoxFit.cover,
                          )))),

               /* Positioned(
                    right: 12.w,
                    top: 0,
                    child: IconButton(icon: SvgPicture.asset("assets/images/icons/edit.svg",
                    color: Colors.black,
                    ),onPressed: (){
                      save_images_shop(widget.store.images.indexOf(im));
                    },))*/


              ]))
                  .toList()),
          Container(
            height: 16.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 20.w,
              ),
              Expanded(
                  child: ToggleBar(
                      labels: ["Fixed Price", "Auction"],
                      onSelectionUpdated: (index) => {
                            if (index == 0)
                              {setState(() => fixed = true)}
                            else
                              setState(() => fixed = false)

                            // Do something with index
                          })),
              Container(
                width: 20.w,
              )
            ],
          ),
          Container(
            height: 12.h,
          ),
          cats.isEmpty
              ? Center(child: CupertinoActivityIndicator())
              : Padding(
                  padding: EdgeInsets.only(left: ScreenUtil().setWidth(24)),
                  child: ButtonsTabBar(
                    controller: _tabController,
                    backgroundColor: Config.col_grey_d,
                    radius: 24,
                    unselectedBackgroundColor: const Color(0xffF1F0F5),
                    unselectedLabelStyle: TextStyle(color: Colors.black),
                    labelStyle: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    tabs: cats.map((e) => Tab(text: e.name)).toList()
                  )),
          Container(height: 15.h),
          fixed == true
              ? GridView.count(
                  childAspectRatio: 166.w / 230.h,
                  crossAxisCount: 2,
                  physics: NeverScrollableScrollPhysics(),
                  // to disable GridView's scrolling
                  shrinkWrap: true,
                  // You won't see infinite size error
                  children: widget.store.bids
                      .map((e) => ProductStoreItem(e))
                      .toList(),
                )
              : Container(),
          fixed == false
              ? GridView.count(
                  childAspectRatio: 166.w / 230.h,

                  crossAxisCount: 2,
                  physics: NeverScrollableScrollPhysics(),
                  // to disable GridView's scrolling
                  shrinkWrap: true,
                  // You won't see infinite size error
                  children: widget.store.products
                      .map((e) => ProductStoreItem(e))
                      .toList(),
                )
              : Container(),
        ],
      ),
    );
  }
}

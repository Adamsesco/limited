import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/reset_password/forget_password.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/services/validators.dart';
import 'package:limited/src/signup/register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:easy_localization/easy_localization.dart';

class Login extends StatefulWidget {
  Login(this.phone);

  String phone;

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  FocusNode _focuspass = new FocusNode();
  FocusNode _focusname = new FocusNode();
  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();
  bool load = false;

  submit_name() {}

  submit_password() {}

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() async {
    //  var token = await      _firebaseMessaging.getToken();

    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      // showInSnackBar("");
    } else {
      setState(() {
        show = true;
      });
      form.save();
      /**/
      String name = _nameController.text;
      String pass = _passwordController.text;

      var a = await restservice.post_login(
          "Login/",
          context,
          json.encode({
            "phone": "$name",
            "password": "$pass",
            // "device_token":"$token"
          }));

      setState(() {
        show = false;
      });


      /* if (a == 201) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("هذا الحساب غير مسجل"),
          duration: new Duration(seconds: 4),
        ));
      }

      else */
      if (a == 400) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("This account is not registered or not existed ! "),
          duration: new Duration(seconds: 4),
        ));
      } else {
        //Routes.goto(context, "login_new")
        //Routes.gof(context,_tel.text, widget.onLocaleChange,func: widget.func);

      }
    }
  }

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  Future<String> _fcmSetupAndGetToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await _firebaseMessaging.getToken().then((token) {
      prefs.setString("token", token);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _nameController.text = widget.phone;

    _fcmSetupAndGetToken();
  }

  Widget textfield(String name, TextEditingController texted, FocusNode focus,
          validator, TextInputType typeinput,
          {obs}) =>
      TextFormField(
          obscureText: obs == null ? false : obs,
          focusNode: focus,
          keyboardType: typeinput,
          decoration: InputDecoration(
            filled: true,
            fillColor: const Color(0xffFAFAFA),
            focusColor: const Color(0xffFAFAFA),
            hintText: name,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: const Color(0xffFAFAFA),
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                  width: 2.0,
                )),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(28),
            ),
            // labelText: name,
            contentPadding:
                EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
          ),
          textAlignVertical: TextAlignVertical.top,
          minLines: 1,
          maxLines: 1,
          controller: texted,
          validator: validator);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget textfield_name = textfield("Phone number", _nameController,
        _focusname, ap.validatename, TextInputType.text);

    Widget textfield_password = textfield(
      "Password",
      _passwordController,
      _focuspass,
      ap.validatePassword,
      TextInputType.text,
      obs: true,
    );

    TextStyle text1_btn = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w700);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: ListView(
        padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
        children: <Widget>[
          Container(
            height: ScreenUtil().setHeight(24),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              )
            ],
          ),
          Container(
            height: ScreenUtil().setHeight(8),
          ),
          Container(
              width: ScreenUtil().setWidth(136),
              height: ScreenUtil().setHeight(136),
              child: SvgPicture.asset("assets/images/icons/logo.svg")),
          Container(
            height: ScreenUtil().setHeight(16),
          ),
          Center(
            child: Text(
              "login_t".tr(),
              style: TextStyle(color: Config.col_grey_d, fontSize: 13),
            ),
          ),
          Container(
            height: ScreenUtil().setHeight(37),
          ),
          new Form(
              key: _formKey,
              autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(38),
                          right: ScreenUtil().setWidth(42),
                          top: ScreenUtil().setHeight(26)),
                      child: textfield_name,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(38),
                          right: ScreenUtil().setWidth(42),
                          top: ScreenUtil().setHeight(26)),
                      child: textfield_password,
                    ),
                    Container(
                      height: ScreenUtil().setHeight(28),
                    ),
                    Container(
                        height: ScreenUtil().setHeight(42),
                        width: ScreenUtil().setWidth(212),
                        child: RaisedButton(
                            color: Config.col_blue,
                            elevation: 1,
                            shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(28),
                              ),
                            ),
                            onPressed: () {
                              _handleSubmitted();
                            },
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  show //CupertinoActivityIndicator()
                                      ? Theme(
                                          data: ThemeData(
                                              cupertinoOverrideTheme:
                                                  CupertinoThemeData(
                                                      brightness:
                                                          Brightness.dark)),
                                          child: CupertinoActivityIndicator())
                                      : Container(),
                                  show //CupertinoActivityIndicator()
                                      ? Container(
                                          width: 4,
                                        )
                                      : Container(),
                                  Text(
                                    "LOGIN",
                                    style: text1_btn,
                                  ),
                                ]))),
                    /* Container(
                        child: GradientButton(
                      increaseWidthBy: MediaQuery.of(context).size.width,
                      increaseHeightBy: ScreenUtil().setHeight(42),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          show
                              ? Center(
                                  child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                ))
                              : Container(),
                          Container(
                            width: 8,
                          ),
                          Text(
                            'دخول',
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                      ),
                      callback: () {
                        _handleSubmitted();
                      },

                    )),*/
                  ],
                ),
              )),
          InkWell(
            child: Container(
              padding: EdgeInsets.only(top: ScreenUtil().setHeight(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'forgot password?'.tr(),
                    style: TextStyle(
                        color: Config.col_blue.withOpacity(0.5), fontSize: 16),
                  )
                ],
              ),
            ),
            onTap: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return ForgetPassword();
              }));
            },
          ),
          Container(
            height: ScreenUtil().setHeight(55),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
                height: ScreenUtil().setHeight(44),
                width: ScreenUtil().setWidth(212),
                child: RaisedButton(
                  color: Colors.white,
                  elevation: 1,
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(28),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return Register(_nameController.text);
                    }));
                  },
                  child: Text(
                    'sign up'.tr(),
                    style: TextStyle(
                        color: Config.col_blue,
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                  ),
                ))
          ])
        ],
      ),
    );
  }
}

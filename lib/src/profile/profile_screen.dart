import 'dart:convert';
import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:limited/src/common/widgets/custom_dialog.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/store/my_store.dart';
import 'package:limited/src/store/mypurchases.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/market/market_detail.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/services/settings_services.dart';
import 'package:limited/src/settings/bid_history.dart';
import 'package:limited/src/settings/payment_setting.dart';
import 'package:limited/src/settings/settings_list.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen(this.go_to_message, this.scaffoldKey);

  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var go_to_message;

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  TextEditingController _passwordController1 = new TextEditingController();
  FocusNode _focuspass = new FocusNode();
  FocusNode _focuspass1 = new FocusNode();
  RestService restservice = new RestService();
  bool show = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _autovalidate = false;
  FocusNode _focusname = new FocusNode();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  Settings_Services settings = new Settings_Services();

  go_market() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return MyMarket();
    }));
  }


  go_shop() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
          return MyStore();
        }));
  }


  void showAlertDialog() async {
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CustomAlertDialog(
          content: Container(
            width: 321.w,
            height: 384.h,
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: const Color(0xFFFFFF),
              borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
            ),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 48.h,
                ),
                Center(
                  child: SvgPicture.asset("assets/images/market.svg"),
                ),
                Padding(
                    padding: EdgeInsets.only(
                        left: 20.w, right: 32.w, top: 26.h, bottom: 32.h),
                    child: Text(
                      "You can now switch your account to a shop "
                      "and take advantage of:",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.0.sp,
                      ),
                      textAlign: TextAlign.center,
                    )),
                Padding(
                    padding: EdgeInsets.only(left: 20.w, right: 32.w),
                    child: Row(
                      children: [
                        Center(
                          child: SvgPicture.asset("assets/images/najma.svg"),
                        ),
                        Container(width: 8.w),
                        Text(
                          "Get  link to your shop",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 14.0.sp,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(left: 20.w, right: 32.w, top: 4.h),
                    child: Row(
                      children: [
                        Center(
                          child: SvgPicture.asset("assets/images/najma.svg"),
                        ),
                        Container(width: 8.w),
                        Text(
                          "Download reports",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 14.0.sp,
                          ),
                        )
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: 20.w, right: 32.w, top: 4.h, bottom: 32.h),
                    child: Row(
                      children: [
                        Center(
                          child: SvgPicture.asset("assets/images/najma.svg"),
                        ),
                        Container(width: 8.w),
                        Text(
                          "Organize your items",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 14.0.sp,
                          ),
                        )
                      ],
                    )),
                MaterialButton(
                  onPressed: () async {
                    Navigator.of(context).pop();
                    ProductSServices rst = new ProductSServices();
                    rst.switch_to_shop();
                    widget.scaffoldKey.currentState.showSnackBar(new SnackBar(
                        backgroundColor: Config.col_blue,
                        content: new Text("Switched to Shop !")));
                  },
                  child: Container(
                    width: 295.w,
                    height: 57.h,
                    child: Material(
                        color: Config.col_blue,
                        borderRadius: BorderRadius.circular(25.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'SWITCH NOW',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.0.sp,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  go_settings() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return SettingsList();
    }));
  }

  bool load = true;
  User user = new User("", "", "", "", DateTime.now(), "", "");

  getUserinfo() async {
    setState(() {
      load = true;
    });

    var a = await settings.user_info();
    if (!this.mounted) return;
    setState(() {
      user = a;
      load = false;
      _nameController.text = user.name;
      _telController.text = user.mobile;
      _emailController.text = user.email;
    });

    if (user.type.toString() != "1") {
      showAlertDialog();
    }
  }

  @override
  void initState() {
    super.initState();
    getUserinfo();
  }

  Future _cropImage(image) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
    );
    if (croppedFile != null) {
      image = croppedFile;
      //ImageProperties properties = await FlutterNativeImage.getImageProperties(image.path);
      File compressedFile =
          await FlutterNativeImage.compressImage(image.path, quality: 70);
      save_image(compressedFile);
    }
  }

  bool uploading = false;

  save_image(image) async {
    setState(() {
      uploading = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("login_token");
    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + ".jpg");
    await storageReference.put(image).onComplete.then((val) {
      val.ref.getDownloadURL().then((val) async {
        var js = json.encode({
          "avatar": val.toString(),
          "token": token,
          "username": user.userename,
          "name": user.name,
          "phone": user.mobile,
          "email": user.email
          // "password": user.password
        });

        var a = await restservice.post("editprofile/", js);
        print(a);

        if (!mounted) return;

        setState(() {
          user.image = val.toString();
          var myProvider = Provider.of<LoginProvider>(context, listen: false);

          myProvider.user = User(
              user.id,
              _nameController.text,
              user.image,
              "",
              user.created,
              _telController.text,
              user.userename);
          //update();
          uploading = false;
        });
      });
    });
  }

  _handleCameraButtonPressed() async {
    Navigator.of(context).pop(true);
    var image = await ImagePicker.platform.pickImage(source: ImageSource.camera);
    _cropImage(File(image.path));
  }

  _handleGalleryButtonPressed() async {
    Navigator.of(context).pop(true);
    PickedFile image = await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    _cropImage(File(image.path));
  }

  open_bottomsheet() {
    showModalBottomSheet<bool>(
        context: context,
        builder: (BuildContext context) {
          return new Container(
              height: 112.0,
              child: new Container(
                  // padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                    new ListTile(
                        onTap: _handleCameraButtonPressed,
                        title: new Text("Take picture")),
                    new ListTile(
                        onTap: _handleGalleryButtonPressed,
                        title: new Text("Gallery")),
                  ])));
        });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: Config.col_blue,
        fontSize: ScreenUtil().setSp(17),
        fontWeight: FontWeight.w800);

    TextStyle text1_btn = TextStyle(
        color: Config.col_blue,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);
    TextStyle text2 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(10),
        fontWeight: FontWeight.w400);

    TextStyle text3 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );

    Widget row_widget(String text, String image, onPressed) => InkWell(
          child: Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(12),
                bottom: ScreenUtil().setHeight(12),
                left: ScreenUtil().setWidth(12),
                right: ScreenUtil().setWidth(12),
              ),
              child: Row(children: [
                SvgPicture.asset(image),
                Container(
                  width: ScreenUtil().setWidth(12),
                ),
                Expanded(
                    child: Text(
                  text,
                  style: text4,
                )),
                SvgPicture.asset("assets/images/icons/arrow.svg"),
              ])),
          onTap: () {
            onPressed();
          },
        );

    return ListView(
      children: [
        Column(
          children: [
            GestureDetector(
              child: SizedBox(
                  height: ScreenUtil().setWidth(82),
                  width: ScreenUtil().setWidth(82),
                  child: Stack(
                    children: <Widget>[
                      CircleAvatar(
                        radius: ScreenUtil().setWidth(43.5),
                        child: ClipOval(
                          child: Image.network(
                            user.image,
                            height: ScreenUtil().setWidth(87),
                            width: ScreenUtil().setWidth(87),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      uploading
                          ? Align(
                              alignment: Alignment.center,
                              child: CupertinoActivityIndicator(),
                            )
                          : Container(),
                      Positioned(
                          top: 1,
                          right: 1,
                          child: Container(
                            height: 32,
                            width: 32,
                            padding: EdgeInsets.all(8),
                            child: SvgPicture.asset(
                              "assets/images/icons/camera.svg",
                              color: Colors.black,
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                          )),
                    ],
                  )),
              onTap: () {
                open_bottomsheet();
              },
            ),
            SizedBox(
              height: ScreenUtil().setHeight(17),
            ),
            Text(user.name, style: text1),
            Container(
              height: 2,
            ),
            Text(
              "Member since " +
                  new DateFormat('dd-MM-yyyy').format(user.created),
              style: text2,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(8),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  "assets/images/icons/adr.svg",
                  width: 12,
                ),
                Container(
                  width: 5,
                ),
                Text(
                  user.addresess == null ? "---" : user.addresess,
                  style: text3,
                )
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(30),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  height: ScreenUtil().setHeight(46),
                  child: RaisedButton(
                    color: Colors.white,
                    elevation: 1,
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(28),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return BidHistory();
                      }));
                    },
                    child: Text(
                      "My Bids",
                      style: text1_btn,
                    ),
                  )),
              SizedBox(
                width: ScreenUtil().setWidth(20),
              ),
              Container(
                  height: ScreenUtil().setHeight(46),
                  child: RaisedButton(
                    color: Colors.white,
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(28),
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return MyPurchases();
                      }));
                    },
                    child: Text(
                      "My Purchases",
                      style: text1_btn,
                    ),
                  )),
            ]),
            SizedBox(
              height: ScreenUtil().setHeight(30),
            ),

            divid,
            row_widget("Market", "assets/images/icons/store.svg", go_market),
            user.type.toString() == "1"
                ?  divid:Container(),
            user.type.toString() == "1"
                ? row_widget(
                "My Shop", "assets/images/icons/store.svg", go_shop)
                : Container(),
            divid,

            row_widget("Messages", "assets/images/icons/msg2.svg",
                widget.go_to_message),
            divid,
            row_widget(
                "Settings", "assets/images/icons/settings.svg", go_settings),
            divid,
            row_widget("Help", "assets/images/icons/help.svg", null),
            divid,
            SizedBox(
              height: ScreenUtil().setHeight(50),
            ),
            Container(
                width: ScreenUtil().setWidth(185),
                height: ScreenUtil().setHeight(42),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                      side: BorderSide(color: Config.col_blue)),
                  color: Colors.white,
                  textColor: Config.col_blue,
                  padding: EdgeInsets.all(8.0),
                  onPressed: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();

                    String id = prefs.getString("id");
                    FirebaseDatabase.instance
                        .reference()
                        .child("status")
                        .child(id)
                        .update({"online": false});

                    var myProvider =
                        Provider.of<LoginProvider>(context, listen: false);

                    if (myProvider.isAuthenticate) {
                      myProvider.mitexto = 'Provider';

                      myProvider.isAuthenticate = false;
                      //StorageUtil.putBool('isAutenticado', null);
                      prefs.clear();
                    }
                  },
                  child: Text(
                    "Sign out",
                    style: text1_btn,
                  ),
                )),
          ],
        )
      ],
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/services/validators.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilee extends StatefulWidget {
  EditProfilee(this.user);

  User user;

  @override
  _EditProfileeState createState() => _EditProfileeState();
}

class _EditProfileeState extends State<EditProfilee> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _usernameController = new TextEditingController();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  FocusNode _focususername = new FocusNode();

  FocusNode _focuspass = new FocusNode();
  FocusNode _focusname = new FocusNode();
  TextEditingController _emailController = new TextEditingController();

  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();
  bool _isComposing1 = false;
  bool _isComposing2 = false;
  bool uploading1 = false;
  bool uploading3 = false;

  var im = "";
  var lo = false;
  List<String> images = new List<String>();

  submit_name() {}

  submit_email() {}

  save_images_shop(image) async {
    setState(() {
      uploading1 = true;
      lo = false;
      _isComposing1 = true;
    });

    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + ".jpg");
    StorageUploadTask uploadTask = storageReference.put(image);
    await storageReference.put(image).onComplete.then((val) {
      val.ref.getDownloadURL().then((val) {
        //if (!mounted) return;

        setState(() {
          images.add(val.toString());
          im = val.toString();
          uploading1 = false;
          lo = true;
        });
      });
    });
  }

  submit_tel() {}

  submit_password() {}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _usernameController.text = widget.user.userename;
    _nameController.text = widget.user.name;
    _emailController.text = widget.user.email;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  Future _cropImage(image) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
    );
    if (croppedFile != null) {
      image = croppedFile;
      //ImageProperties properties = await FlutterNativeImage.getImageProperties(image.path);
      File compressedFile =
          await FlutterNativeImage.compressImage(image.path, quality: 60);
      save_image(compressedFile);
    }
  }

  bool uploading = false;

  save_profil(context) async {
    setState(() {
      uploading3 = true;
    });
    var myProvider = Provider.of<LoginProvider>(context, listen: false);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("login_token");

    var js = json.encode({
      "avatar": widget.user.image,
      "token": token,
      "username": _usernameController.text,
      "name": _nameController.text,
      "phone": _telController.text,
      "email": _emailController.text,
      // "password": user.password
    });

    var a = await restservice.post("editprofile/", js);
    print(a);

    if (!mounted) return;

    myProvider.user = User(
        widget.user.id,
        _nameController.text,
        widget.user.image,
        "",
        widget.user.created,
        _telController.text,
        _usernameController.text);

    setState(() {
      uploading3 = false;
    });
  }

  save_image(image) async {
    var myProvider = Provider.of<LoginProvider>(context, listen: false);

    setState(() {
      uploading = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("login_token");
    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + ".jpg");
    await storageReference.put(image).onComplete.then((val) {
      val.ref.getDownloadURL().then((val) async {
        var js = json.encode({
          "username": widget.user.userename,
          "name": widget.user.name,
          "avatar": val.toString(),
          "token": token,
          "phone": widget.user.mobile,
          "email": widget.user.email
          // "password": user.password
        });

        var a = await restservice.post("editprofile/", js);

        if (!mounted) return;

        setState(() {
          widget.user.image = val.toString();
          //update();
          uploading = false;
        });

        myProvider.user = User(
            widget.user.id,
            widget.user.userename,
            val.toString(),
            "",
            widget.user.created,
            widget.user.mobile,
            widget.user.userename);
      });
    });
  }

  _handleCameraButtonPressed() async {
    Navigator.of(context).pop(true);
    var image = await ImagePicker.platform.pickImage(source: ImageSource.camera);
    _cropImage(File(image.path));
  }

  _handleGalleryButtonPressed() async {
    Navigator.of(context).pop(true);
    var image = await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    _cropImage(File(image.path));
  }

  open_bottomsheet() {
    showModalBottomSheet<bool>(
        context: context,
        builder: (BuildContext context) {
          return new Container(
              height: 112.0,
              child: new Container(
                  // padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                    new ListTile(
                        onTap: _handleCameraButtonPressed,
                        title: new Text("Take picture")),
                    new ListTile(
                        onTap: _handleGalleryButtonPressed,
                        title: new Text("Gallery")),
                  ])));
        });
  }

  @override
  Widget build(BuildContext context) {
    Widget textfield(String name, TextEditingController texted, FocusNode focus,
            validator, TextInputType typeinput,
            {obs}) =>
        TextFormField(
            obscureText: obs == null ? false : obs,
            focusNode: focus,
            keyboardType: typeinput,
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xffFAFAFA),
              focusColor: const Color(0xffFAFAFA),
              hintText: name,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: const Color(0xffFAFAFA),
                    width: 2.0,
                  )),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              // labelText: name,
              contentPadding:
                  EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
            ),
            textAlignVertical: TextAlignVertical.top,
            minLines: 1,
            maxLines: 1,
            controller: texted,
            validator: validator);
    List<Widget> widgets() {
      return images
          .map((String file) => new Stack(children: <Widget>[
                new Container(
                    padding: new EdgeInsets.all(4.0),
                    height: ScreenUtil().setHeight(94),
                    width: ScreenUtil().setWidth(97),
                    child: ClipRRect(
                        //<--clipping image
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        child: new Image.network(
                          file,
                          fit: BoxFit.cover,
                        ))),
                new Positioned(
                    bottom: 0.0,
                    left: 0.0,
                    child: new InkWell(
                      child: new Center(
                          child: new SvgPicture.asset(
                        "assets/images/remove.svg",
                      )),
                      onTap: () {
                        setState(() {
                          images.remove(file);
                        });
                      },
                    ))
              ]))
          .toList();
    }

    getpictues() {
      return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        InkWell(
            onTap: () async {
              PickedFile _imageFile =
                  await ImagePicker.platform.pickImage(source: ImageSource.gallery);
              File compressedFile = await FlutterNativeImage.compressImage(
                  _imageFile.path,
                  quality: 70);

              save_images_shop(compressedFile);
            },
            child: Container(
                padding: new EdgeInsets.all(4.0),
                height: ScreenUtil().setHeight(94),
                width: ScreenUtil().setWidth(97),
                child: Material(
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                    ),
                    //<--clipping image
                    child: uploading1
                        ? CupertinoActivityIndicator()
                        : Center(
                            child: Icon(
                            Icons.add,
                            size: 34,
                          ))))),
        new Container(
            height: ScreenUtil().setHeight(87),
            child: new Row(children: widgets()))
      ]);
    }

    Widget textfield_name = textfield("Full name", _nameController, _focusname,
        ap.validatename, TextInputType.text);

    Widget textfield_username = textfield("Username", _usernameController,
        _focususername, ap.validatename, TextInputType.text);

    Widget textfield_password = textfield(
      "Password",
      _passwordController,
      _focuspass,
      ap.validatePassword,
      TextInputType.text,
      obs: true,
    );

    Widget textfield_email = textfield("Email", _emailController, _focusemail,
        ap.validateEmail, TextInputType.emailAddress);

    Widget textfield_tel = textfield("Mobile number", _telController, _focustel,
        ap.validatephonenumber, TextInputType.phone);

    TextStyle text1_btn = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w700);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: LAppBar(
        true,
        leading_widget: IconButton(
          icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
        children: <Widget>[
          Container(
            height: ScreenUtil().setHeight(24),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                child: SizedBox(
                    height: ScreenUtil().setWidth(82),
                    width: ScreenUtil().setWidth(82),
                    child: Stack(
                      children: <Widget>[
                        CircleAvatar(
                          radius: ScreenUtil().setWidth(43.5),
                          child: ClipOval(
                            child: Image.network(
                              widget.user.image,
                              height: ScreenUtil().setWidth(87),
                              width: ScreenUtil().setWidth(87),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        uploading
                            ? Align(
                                alignment: Alignment.center,
                                child: CupertinoActivityIndicator(),
                              )
                            : Container(),
                        Positioned(
                            top: 1,
                            right: 1,
                            child: Container(
                              height: 32,
                              width: 32,
                              padding: EdgeInsets.all(8),
                              child: SvgPicture.asset(
                                "assets/images/icons/camera.svg",
                                color: Colors.black,
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            )),
                      ],
                    )),
                onTap: () {
                  open_bottomsheet();
                },
              ),
            ],
          ),
          SizedBox(
            height: ScreenUtil().setHeight(17),
          ),
          Container(
            height: ScreenUtil().setHeight(16),
          ),
          new Form(
            key: _formKey,
            autovalidate: _autovalidate,
            //onWillPop: _warnUserAboutInvalidData,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_name,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_username,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_password,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_email,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_tel,
                ),
                Container(
                  height: ScreenUtil().setHeight(16),
                ),
               // getpictues(),
                Container(
                    height: ScreenUtil().setHeight(42),
                    width: ScreenUtil().setWidth(185),
                    child: RaisedButton(
                        color: Config.col_blue,
                        elevation: 1,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(28),
                          ),
                        ),
                        onPressed: () {
                          save_profil(context);
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? Theme(
                                      data: ThemeData(
                                          cupertinoOverrideTheme:
                                              CupertinoThemeData(
                                                  brightness: Brightness.dark)),
                                      child: CupertinoActivityIndicator())
                                  /*valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Colors.white),
                                    )*/
                                  : Container(),
                              uploading3
                                  ? CupertinoActivityIndicator()
                                  : Container(),
                              Text(
                                "REGISTER",
                                style: text1_btn,
                              ),
                            ]))),
                /* Container(
                        child: GradientButton(
                      increaseWidthBy: MediaQuery.of(context).size.width,
                      increaseHeightBy: ScreenUtil().setHeight(40),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            show
                                ? Center(
                                    child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white),
                                  ))
                                : Container(),
                            Container(
                              width: 8,
                            ),
                            Text(
                              'دخول',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            )
                          ]),
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                      ),
                      callback: () {
                        _handleSubmitted();
                      },

                    )),*/
              ],
            ),
          ),
          Container(
            height: ScreenUtil().setHeight(16),
          ),
          /*  Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: new RichText(
                    text: new TextSpan(
                      text: "بالتسجيل ، فأنت توافق على الالتزام بهذه  ",
                      style: new TextStyle(
                          color: Config.col_grey_medium,
                          fontFamily: "arb",
                          fontSize: ScreenUtil().setSp(9)),
                      children: <TextSpan>[
                        new TextSpan(
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Terms())),
                            text: "الشروط والأحكام ",
                            style: new TextStyle(
                                color: Config.col_blue,
                                fontWeight: FontWeight.bold)),
                        new TextSpan(
                            text: "  و على  ",
                            style:
                                new TextStyle(color: Config.col_grey_medium)),
                        new TextSpan(
                            text: " سياسة الخصوصية. ",
                            style: new TextStyle(
                                color: Config.col_blue,
                                fontWeight: FontWeight.bold),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Privacy()))),
                      ],
                    ),
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "هل لديك حساب؟",
                style: TextStyle(color: Config.col_green),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "سجل دخولك",
                  style: TextStyle(
                      color: Config.col_blue, fontWeight: FontWeight.w700),
                ),
              )
            ],
          )*/
        ],
      ),
    );
  }
}

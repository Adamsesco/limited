import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PaymentMethod extends StatefulWidget {
  @override
  _PaymentMethodState createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  var codeCtrl = TextEditingController();
  RestService rest = new RestService();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool load = false;

  void showInSnackBar(String value, color) {
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(backgroundColor: color, content: new Text(value)));
  }

  _savetext() async {
    print("gdgdgdggdgdgdgdggdgd");
    if (codeCtrl.text != "") {
      final FormState form = _formKey.currentState;
      print(form.validate());

      if (!form.validate()) {
        _autovalidate = true; // Start validating on every change.
        //showInSnackBar("Correct ");
      } else {
        form.save();

        setState(() {
          load = true;
        });
        SharedPreferences prefs = await SharedPreferences.getInstance();

        String id = prefs.getString("login_token");
        print(id);
        var a = await rest.post(
            "updateIban/", json.encode({"iban": codeCtrl.text, "token": id}));
        //print(a);
        if (a["status"] == 200) {
          showInSnackBar(a["message"], Config.col_blue);

          setState(() {
            load = false;
          });
          new Timer(const Duration(seconds: 2), () {
            Navigator.pop(context);
          });
        } else {
          showInSnackBar(a["message"], Colors.red[800]);
        }
      }
    }
    else {
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("yessss");
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(22),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(22),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w500),
        ));

    Widget textfield(String name, TextEditingController texted) =>
        TextFormField(
          maxLength: 27,
          decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xffFAFAFA),
              focusColor: const Color(0xffFAFAFA),
              hintText: name,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: const Color(0xffFAFAFA),
                    width: 2.0,
                  )),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              // labelText: name,
              contentPadding:
                  EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
              suffixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: ScreenUtil().setWidth(9.5),
                  backgroundColor: const Color(0xffF1F0F5),
                  child: IconButton(
                      icon: Icon(Icons.close, size: 18),
                      onPressed: () {
                        texted.clear();
                      }),
                ),
              )),
          textAlignVertical: TextAlignVertical.top,
          minLines: 1,
          maxLines: 1,
          controller: texted,
          validator: (value) {
            if (value.length != 27) return 'IBAN is NOT VALID';
            return null;
          },
        );

    return WillPopScope(
        onWillPop: () async {
          print("hdhdhdhd");
          await _savetext();
        },
        child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            appBar: LAppBar(
              true,
              leading_widget: load?CupertinoActivityIndicator():IconButton(
                icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
                onPressed: () {
                  _savetext();
                },
              ),
            ),
            body: Form(
                key: _formKey,
                autovalidate: _autovalidate,
                //onWillPop: _warnUserAboutInvalidData,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      text("Payment method"),
                      Container(
                        height: 2,
                      ),
                      text2("Write below your bank account number (IBAN)"),
                      Container(
                        height: ScreenUtil().setHeight(16),
                      ),
                      Container(
                          margin: EdgeInsets.only(
                            left: ScreenUtil().setWidth(16),
                            right: ScreenUtil().setWidth(16),
                          ),
                          child: textfield("IBAN", codeCtrl)),
                    ]))));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/settings/address_setting.dart';
import 'package:limited/src/settings/notifications_settings.dart';
import 'package:limited/src/settings/payment_setting.dart';

class SettingsList extends StatefulWidget {
  @override
  _SettingsListState createState() => _SettingsListState();
}

class _SettingsListState extends State<SettingsList> {
  go_pay() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return PaymentSettings();
    }));
  }

  go_adr() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return AddressList();
    }));
  }

  go_notif() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return NotificationsSetting();
    }));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(29),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(String text, String image, onPressed) => InkWell(
          child: Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(18),
                bottom: ScreenUtil().setHeight(18),
                left: ScreenUtil().setWidth(12),
                right: ScreenUtil().setWidth(12),
              ),
              child: Row(children: [
                SvgPicture.asset(image),
                Container(
                  width: ScreenUtil().setWidth(12),
                ),
                Expanded(
                    child: Text(
                  text,
                  style: text4,
                )),
                SvgPicture.asset("assets/images/icons/arrow.svg"),
              ])),
          onTap: () {
            onPressed();
          },
        );

    return Scaffold(
        appBar: LAppBar(true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/menu.svg"),
            onPressed: () {},
          ),
        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          text("Settings"),
          Expanded(
              child: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(12),
                right: ScreenUtil().setWidth(12)),
            children: [
              divid,
              row_widget(
                  "Notifications", "assets/images/icons/notif2.svg", go_notif),
              divid,
              row_widget("Security", "assets/images/icons/security.svg", null),
              divid,
              row_widget("Address", "assets/images/icons/adress.svg", go_adr),
              divid,
              row_widget("Payments", "assets/images/icons/pay.svg", go_pay),
              divid,
              row_widget("About", "assets/images/icons/about.svg", null),
              divid,
            ],
          ))
        ]));
    ;
  }
}

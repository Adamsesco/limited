import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';

class NotificationsSetting extends StatefulWidget {
  @override
  _NotificationsSettingState createState() => _NotificationsSettingState();
}

class _NotificationsSettingState extends State<NotificationsSetting> {
  bool switch1 = false;
  bool switch2 = true;
  bool switch3 = false;
  bool switch4 = true;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(16),
        fontWeight: FontWeight.w500);

    TextStyle text5 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(10.5),
        fontWeight: FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(6),
        bottom: ScreenUtil().setHeight(6),
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(String text1, String text2, onPressed, bool swit) =>
        Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(12),
              bottom: ScreenUtil().setHeight(12),
              left: ScreenUtil().setWidth(21),
              right: ScreenUtil().setWidth(12),
            ),
            child: Row(children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    text1,
                    style: text4,
                  ),
                  Container(
                    height: ScreenUtil().setHeight(6),
                  ),
                  Container(
                      width: ScreenUtil().setWidth(254),
                      child: Text(
                        text2,
                        style: text5,
                      )),
                ],
              )),
              CupertinoSwitch(
                  activeColor: Config.col_blue,
                  value: swit, onChanged: (val) {
                setState(() {
                  swit=val;
                });
              }),
            ]));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(21),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    return Scaffold(
      appBar: LAppBar(true,
        leading_widget: IconButton(
          icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),

      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          text("Notifications"),
          divid,
          row_widget(
              "All Push Notifications",
              "When turned on, you will receive all notifications without any restriction.",
              null,
              switch1),
          divid,
          row_widget(
              "Order Updates",
              "You will be notified of any purchases or auctions you are in.",
              null,
              switch2),
          divid,

          row_widget(
              "Messages",
              "You will be notified of messages from buyers & sellers.",
              null,
              switch3),
          divid,
          row_widget(
              "New Hot Deals",
              "When turned on you will be informed about The hottest deals on the App.",
              null,
              switch4),
          divid,
        ],
      ),
    );
  }
}

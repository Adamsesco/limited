import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/settings/payment_method.dart';

class PaymentSettings extends StatefulWidget {
  @override
  _PaymentSettingsState createState() => _PaymentSettingsState();
}

class _PaymentSettingsState extends State<PaymentSettings> {
  TextStyle text4 = TextStyle(
      color: Config.col_grey_d,
      fontSize: ScreenUtil().setSp(14),
      fontWeight: FontWeight.w500);

  go_pay() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
      return PaymentMethod();
    }));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget divid = Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(6),
        bottom: ScreenUtil().setHeight(6),
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(String text, onPressed) => InkWell(
        onTap: () {
          go_pay();
        },
        child: Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(12),
              bottom: ScreenUtil().setHeight(12),
              left: ScreenUtil().setWidth(29),
              right: ScreenUtil().setWidth(12),
            ),
            child: Row(children: [
              Expanded(
                  child: Text(
                text,
                style: text4,
              )),
              SvgPicture.asset("assets/images/icons/arrow.svg"),
            ])));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(21),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    return Scaffold(
      appBar: LAppBar(true,
        leading_widget: IconButton(
          icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),

      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          text("Payments"),
          divid,
          row_widget("Add a payment method", null),
          divid,
          row_widget("Add your withdrawal method", null),
          divid,
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';

class EditAddress extends StatefulWidget {
  @override
  _EditAddressState createState() => _EditAddressState();
}

class _EditAddressState extends State<EditAddress> {
  var adrCtrl = TextEditingController();
  var cityCtrl = TextEditingController();
  var codeCtrl = TextEditingController();
  var userCtrl = TextEditingController();

  Widget textfield(String name, TextEditingController texted) => TextFormField(
        decoration: InputDecoration(
            filled: true,
            fillColor: const Color(0xffFAFAFA),
            focusColor: const Color(0xffFAFAFA),
            hintText: name,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: const Color(0xffFAFAFA),
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                  width: 2.0,
                )),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(28),
            ),
            // labelText: name,
            contentPadding:
                EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
            suffixIcon: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                radius: ScreenUtil().setWidth(9.5),
                backgroundColor: const Color(0xffF1F0F5),
                child: IconButton(
                    icon: Icon(Icons.close, size: 18),
                    onPressed: () {
                      texted.clear();
                    }),
              ),
            )),
        textAlignVertical: TextAlignVertical.top,
        minLines: 1,
        maxLines: 1,
        controller: texted,
        validator: (value) {
          if (value.isEmpty) return 'Value is empty';
          return null;
        },
      );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(21),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Row(
          children: [
            Text(
              text,
              style: TextStyle(
                  color: Config.col_grey_d,
                  fontSize: ScreenUtil().setSp(17),
                  fontWeight: FontWeight.w900),
            ),
            Expanded(
              child: Container(),
            ),
            FlatButton(
                onPressed: () {},
                child: Text(
                  "Done",
                  style: TextStyle(
                      color: Config.col_blue,
                      fontWeight: FontWeight.w900,
                      fontSize: ScreenUtil().setSp(14)),
                ))
          ],
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),

        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          text("Address"),
          Expanded(
              child: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(12),
                right: ScreenUtil().setWidth(12)),
            children: [
              textfield("Username", userCtrl),
              Container(
                height: ScreenUtil().setHeight(21),
              ),
              textfield("Address", adrCtrl),
              Container(
                height: ScreenUtil().setHeight(21),
              ),
              textfield("City", cityCtrl),
              Container(
                height: ScreenUtil().setHeight(21),
              ),
              textfield("Code postal", codeCtrl),
              Container(
                height: ScreenUtil().setHeight(21),
              ),
            ],
          ))
        ]));
    ;
  }
}

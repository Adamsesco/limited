import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/settings/edit_address.dart';

class AddressList extends StatefulWidget {
  @override
  _AddressListState createState() => _AddressListState();
}

class _AddressListState extends State<AddressList> {


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(16),
        fontWeight: FontWeight.w500);

    TextStyle textname = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(12.5),
        fontWeight: FontWeight.w900);

    TextStyle text5 = TextStyle(
        color: Config.col_grey_dark,
        fontSize: ScreenUtil().setSp(13.0),
        fontWeight: FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(6),
        bottom: ScreenUtil().setHeight(6),
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(String name, String address,String city, String code, onPressed) =>
        InkWell(child: Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(12),
              bottom: ScreenUtil().setHeight(12),
              left: ScreenUtil().setWidth(21),
              right: ScreenUtil().setWidth(12),
            ),
            child: Row(children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        name,
                        style: textname,
                      ),
                      Container(
                        height: ScreenUtil().setHeight(6),
                      ),
                      Container(
                          width: ScreenUtil().setWidth(254),
                          child: Text(
                            address,
                            style: text5,
                          )),
                      Container(
                        height: ScreenUtil().setHeight(2),
                      ),
                      Container(
                          width: ScreenUtil().setWidth(254),
                          child: Text(
                            city,
                            style: text5,
                          )),
                      Container(
                        height: ScreenUtil().setHeight(2),
                      ),
                      Container(
                          width: ScreenUtil().setWidth(254),
                          child: Text(
                            code,
                            style: text5,
                          )),
                    ],
                  )),
              SvgPicture.asset("assets/images/icons/arrow.svg"),

            ])),onTap: (){
            Navigator.push(context,
                new MaterialPageRoute(builder: (BuildContext context) {
                  return EditAddress();
                }));

        },);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(21),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    return  Scaffold(
        appBar: LAppBar(true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),

        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          text("Address"),
          Expanded(
              child: ListView(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(12),
                    right: ScreenUtil().setWidth(12)),
                children: [
                  divid,
                  row_widget("Adam Daaif", "B5 Number 783 Bensergao","Agadir", "80000", null),
                  divid,

                ],
              ))
        ]));
  }
}

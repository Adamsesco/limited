import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/services/validators.dart';

class ForgetPassword extends StatefulWidget {
  ForgetPassword();

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<ForgetPassword> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  FocusNode _focuspass = new FocusNode();
  FocusNode _focusname = new FocusNode();
  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();
  bool load = false;
  TextEditingController _emailController = new TextEditingController();
  FocusNode _focusemail = new FocusNode();
  bool show1 = false;
  String _text = "";
  bool enter_pass = false;
  String code;

  submit_email() {}

  submit_password() {}

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  check(code) async {
    print("ckeck");
    var a = await restservice.post("forgetPassword/check", {"code": "$code"});

    setState(() {
      show = false;
    });

    print(a);
    if (a["message"] == "Success") {
      setState(() {
        _text = "";
        enter_pass = true;
      });
    } else {
      ///9967
      setState(() {
        setState(() {
          _text = "الرمز غير صحيح";
        });

        show1 = true;
      });
    }
  }

  sendcode() async {
    var a = await restservice.get("forgetPassword/" + _emailController.text);

    setState(() {
      show = false;
    });

    if (a["message"] == "Error ,user not exist") {
      setState(() {
        _text = " لا يوجد لدينا هذا الحساب";
      });
    } else {
      ///9967
      setState(() {
        _text = "";
        show1 = true;
      });
    }
  }

  void _handleSubmitted() async {
    //  var token = await      _firebaseMessaging.getToken();

    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar(" يرجى تصحيح الأخطاء باللون الأحمر قبل الإرسال");
    } else {
      setState(() {
        show = true;
      });
      form.save();

      if (show1 == false) {
        sendcode();
      } else {
        code = _passwordController.text;
        check(_passwordController.text);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget textfield(String name, TextEditingController texted, FocusNode focus,
            validator, TextInputType typeinput, {obs}) =>
        TextFormField(
            obscureText: obs == null ? false : obs,
            focusNode: focus,
            keyboardType: typeinput,
            decoration: InputDecoration(
                filled: true,
                fillColor: const Color(0xffFAFAFA),
                focusColor: const Color(0xffFAFAFA),
                hintText: name,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: const Color(0xffFAFAFA),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: BorderSide(
                      color: const Color(0xffFAFAFA),
                      width: 2.0,
                    )),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(28),
                ),
                // labelText: name,
                contentPadding:
                    EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
                suffixIcon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    radius: ScreenUtil().setWidth(9.5),
                    backgroundColor: const Color(0xffF1F0F5),
                    child: IconButton(
                        icon: Icon(Icons.close, size: 18),
                        onPressed: () {
                          texted.clear();
                        }),
                  ),
                )),
            textAlignVertical: TextAlignVertical.top,
            minLines: 1,
            maxLines: 1,
            controller: texted,
            validator: validator);

    Widget textfield_email = textfield("البريد الإلكتروني", _emailController,
        _focusemail, ap.validateEmail, TextInputType.emailAddress);

    Widget textfield_password = textfield(
      "الرمز",
      _passwordController,
      _focuspass,
      ap.validatePassword2,
      TextInputType.text,
      obs: true,
    );

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: ListView(
        padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
        children: <Widget>[
          Container(
            height: ScreenUtil().setHeight(24),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              )
            ],
          ),
          Container(
            height: ScreenUtil().setHeight(12),
          ),
          Container(
              width: ScreenUtil().setWidth(176),
              height: ScreenUtil().setHeight(176),
              child: Image.asset(
                "assets/images/logo.png",
                fit: BoxFit.contain,
                height: ScreenUtil().setHeight(170),
                width: ScreenUtil().setWidth(170),
              )),
          Container(
            height: ScreenUtil().setHeight(24),
          ),
          Center(
            child: Text(
              "استرجاع كلمة المرور",
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(18),
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            height: 12,
          ),
          enter_pass
              ? Container()
              : Center(
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.52,
                      child: Text(
                        "الرجاء كتابة البريد الالكتروني سيتم ارسال رمزالتفعيل في حالة كان البريد مسجلا لدينا ",
                        style:
                            TextStyle(color: Config.col_grey_d, fontSize: 14),
                      )),
                ),
          Container(
            height: ScreenUtil().setHeight(28),
          ),
          new Form(
              key: _formKey,
              autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Material(
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                elevation: 3,
                child: /*enter_pass? EnterPassword(this.code, this._formKey,this._autovalidate,_scaffoldKey) :*/ Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(38),
                          right: ScreenUtil().setWidth(42),
                          top: ScreenUtil().setHeight(26)),
                      child: textfield_email,
                    ),
                    show1
                        ? Container(
                            height: ScreenUtil().setHeight(26),
                          )
                        : Container(),
                    show1
                        ? Container(
                            height: 1,
                            width: MediaQuery.of(context).size.height,
                            color: Config.col_grey_d)
                        : Container(),
                    show1
                        ? Container(
                            padding: EdgeInsets.only(
                                left: ScreenUtil().setWidth(38),
                                right: ScreenUtil().setWidth(42),
                                top: ScreenUtil().setHeight(26)),
                            child: textfield_password,
                          )
                        : Container(),
                    Container(
                      height: ScreenUtil().setHeight(28),
                    ),
                    _text == ""
                        ? Container()
                        : Center(
                            child: Text(
                              _text,
                              style: TextStyle(color: Colors.red[900]),
                            ),
                          ),
                    /*  Container(
                        child: GradientButton(
                      increaseWidthBy: MediaQuery.of(context).size.width,
                      increaseHeightBy: ScreenUtil.getInstance().setHeight(42),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          show
                              ? Center(
                                  child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                ))
                              : Container(),
                          Container(
                            width: 8,
                          ),
                          Text(
                            show1 ? 'تحقق' : 'ارسال الرمز',
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                      ),
                      callback: () {
                        _handleSubmitted();
                      },
                      gradient: Gradients.backToFuture,
                      shadowColor:
                          Gradients.backToFuture.colors.last.withOpacity(0.25),
                    )),*/
                  ],
                ),
              )),
        ],
      ),
    );
  }
}

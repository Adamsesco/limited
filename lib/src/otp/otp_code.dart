import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/signup/register.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:pin_entry_text_field/pin_entry_text_field.dart';

class OtpCodee extends StatefulWidget {
  OtpCodee(this.phone, this.code);

  String phone;
  int code;

  @override
  _OtpVerificationState createState() => _OtpVerificationState();
}

class _OtpVerificationState extends State<OtpCodee> {
  bool show = false;
  var _phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _phoneController.text = widget.phone;
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          right: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(62),
            right: ScreenUtil().setWidth(62),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            children: [
              Container(
                height: 24.h,
              ),
              Center(child: text("Enter OTP")),
              Container(
                height: 10.h,
              ),
              Center(
                  child: text2(
                "Enter the OTP on your phone"
                "In case you didn’t receive it, try to resend it again.",
              )),
              Container(
                height: 42.h,
              ),
              Container(
                height: 42.h,
              ),
              Center(child: Text("Enter the OTP sent to")),
              Center(child: Text(widget.phone)),
              Container(
                height: 12.h,
              ),
              Center(
                  child: PinEntryTextField(
                      fields: 4,
                      onSubmit: (String pin) {
                        if (pin == widget.code.toString()) {
                          Navigator.push(context, new MaterialPageRoute(
                              builder: (BuildContext context) {
                            return Register("+965"+_phoneController.text);
                          }));
                        }

                      })),
              Container(
                height: 20.h,
              ),
              Text("Didn’t receive the OTP ?"),
              Container(
                height: 20.h,
              ),
              /*  Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: ScreenUtil().setWidth(168),
                      height: ScreenUtil().setHeight(46),
                      child: RaisedButton(
                          color: Config.col_blue,
                          elevation: 1,
                          shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(28),
                            ),
                          ),
                          onPressed: () async {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? CupertinoTheme(
                                      data: CupertinoTheme.of(context).copyWith(
                                          brightness: Brightness.dark),
                                      child: CupertinoActivityIndicator())
                                  : Container(),
                              show //CupertinoActivityIndicator()
                                  ? Container(
                                      width: 4,
                                    )
                                  : Container(),
                              Text(
                                "GO",
                                style: text_btn2,
                              ),
                            ],
                          ))),*/

              /* Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xffF7F6F9),
                  ),
                  width: MediaQuery.of(context).size.width * 0.94,
                  child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Column(children: [])))*/
            ]));
  }
}

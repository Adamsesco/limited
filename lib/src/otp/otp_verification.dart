import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/otp/otp_code.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/signin/signin.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class OtpVerification extends StatefulWidget {
  OtpVerification(

  );


  @override
  _OtpVerificationState createState() => _OtpVerificationState();
}

class _OtpVerificationState extends State<OtpVerification> {
  bool show = false;
  var _phoneController = TextEditingController();

  RestService  rest = new RestService();

  @override
  Widget build(BuildContext context) {
    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          right: ScreenUtil().setWidth(28),

          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(62),
            right: ScreenUtil().setWidth(62),

            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            children: [
              Container(height: 24.h,),
              Center(child: text("OTP Verification")),
              Container(
                height: 10.h,
              ),
              Center(
                  child: text2(
                "We will send you, a One Time Password"
                "to verify your device",
              )),
              Container(
                height: 42.h,
              ),
              Center(child: text("Enter your phone number")),
              Container(
                height: 42.h,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "+965",
                    style: TextStyle(
                        color: Config.col_grey_d,
                        fontSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.bold),
                  ),
                  Container(
                    width: 8.w,
                  ),
                  Container(
                      width: 180.w,
                      child: TextField(
                        keyboardType: TextInputType.phone,
                        controller: _phoneController,
                        decoration: InputDecoration(
                         // labelText: 'Phone number',
                          hintText: 'Phone number',
                         // icon: Icon(Icons.contact_mail),
                        ),
                      )),
                ],
              ),

              Container(
                height: 60.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: ScreenUtil().setWidth(168),
                      height: ScreenUtil().setHeight(46),
                      child: RaisedButton(
                          color: Config.col_blue,
                          elevation: 1,
                          shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(28),
                            ),
                          ),
                          onPressed: () async {

                            setState(() {
                              show= true;
                            });
                            var res = await rest.get("checkPhone/+965"+_phoneController.text);
                            print(res);
                            setState(() {
                              show= false;
                            });

                            if(res["message"]== "Already registred"){
                              Navigator.pop(context);

                              Navigator.push(context,
                                  new MaterialPageRoute(builder: (BuildContext context) {
                                    return Login("+965"+_phoneController.text);
                                  }));
                            }
                            else
                            Navigator.push(context,
                                new MaterialPageRoute(builder: (BuildContext context) {
                                  return OtpCodee(_phoneController.text,res['otp']);
                                }));

                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? CupertinoTheme(
                                      data: CupertinoTheme.of(context).copyWith(
                                          brightness: Brightness.dark),
                                      child: CupertinoActivityIndicator())
                                  : Container(),
                              show //CupertinoActivityIndicator()
                                  ? Container(
                                      width: 4,
                                    )
                                  : Container(),
                              Text(
                                "GET OTP",
                                style: text_btn2,
                              ),
                            ],
                          ))),
                ],
              ),
             /* Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xffF7F6F9),
                  ),
                  width: MediaQuery.of(context).size.width * 0.94,
                  child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Column(children: [])))*/
            ]));
  }
}

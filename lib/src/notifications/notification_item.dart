import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/notification.dart';

class NotificationItem extends StatelessWidget {
  NotificationItem(this.notification);

  LNotification notification;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      ListTile(
        leading: Container(
          width: ScreenUtil().setWidth(47).toDouble(),
          height: ScreenUtil().setWidth(47).toDouble(),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(notification.image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        title: Container(
            child: Text(
          notification.time,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(11.5), color: Config.col_unselected),
        )),
        subtitle: Container(
            padding: EdgeInsets.only(top: 4),
            width: ScreenUtil().setWidth(198),
            child: Text(
              notification.name,
              maxLines: 2,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(13.8), color: Config.col_grey_d),
            )),
        trailing: SvgPicture.asset("assets/images/icons/arrow.svg"),
      ),
      Container(
        margin: EdgeInsets.only(
            top: ScreenUtil().setHeight(20),
            bottom: ScreenUtil().setHeight(20)),
        width: MediaQuery.of(context).size.width,
        height: 1,
        color: Config.col_unselected.withOpacity(0.5),
      )
    ]);
  }
}

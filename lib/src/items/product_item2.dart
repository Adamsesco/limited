import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/detail_product/details_product.dart';

class ProductItem2 extends StatelessWidget {
  ProductItem2(this.product);

  Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: (){
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
                return DetailsProduct(product);
              }));
        },
        child: Container(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(8)),
        child: Column(
      children: [
        Divider(),
        Row(
          children: [
            ClipRRect(
                //<--clipping image

                borderRadius: BorderRadius.all(Radius.circular(16)),
                child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/placeholder.png"),
                            fit: BoxFit.contain)),
                    child:product.image == null
                        ? Container()
                        :  FadeInImage.assetNetwork(
                      image: product.image[0],
                      placeholder: "assets/images/placeholder.png",
                      width: ScreenUtil().setWidth(111),
                      height: ScreenUtil().setHeight(80),
                      fit: BoxFit.cover,
                    ))),
            Container(
              width: ScreenUtil().setWidth(12),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: ScreenUtil().setWidth(220),
                    child: Text(
                      product.name.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w800,
                        fontSize: ScreenUtil().setSp(13.5),
                      ),
                    )),
                Container(
                    height: ScreenUtil().setHeight(40),
                    width: ScreenUtil().setWidth(220),
                    child: Text(
                      product.description.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                Container(
                  height: ScreenUtil().setHeight(0),
                ),
                Container(
                    width: ScreenUtil().setWidth(220),
                    child: Row(
                      children: [
                        Text(product.last_price+ " KD",style: TextStyle(
                            color: Config.col_grey_d,
                            fontSize: ScreenUtil().setSp(15.5),
                            fontWeight: FontWeight.w800
                        ),)
                      ],
                    )),
                /**

                 */


              ],
            ),
          ],
        ),
        Divider(),
      ],
    )));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/home/categories/products_by_category.dart';
import 'package:limited/src/services/categories_services.dart';

class CategoriesList extends StatefulWidget {
  @override
  _CategoriesListState createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList> {
  List<Category> cats = new List<Category>();
  CategoriesService cat = new CategoriesService();
  bool load = true;

  getList() async {
    var a = await cat.get_all_categories();
    if (!this.mounted) return;
    setState(() {
      cats = a;
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  @override
  Widget build(BuildContext context) {
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(29),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    return Scaffold(
        appBar: LAppBar(
          false,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions_widget: Container(),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            text("Browse categories"),
            Expanded(
                child: load == true
                    ? Center(
                        child: CupertinoActivityIndicator(),
                      )
                    : Padding(
                        padding: EdgeInsets.only(
                            left: ScreenUtil().setWidth(8),
                            right: ScreenUtil().setWidth(8)),
                        child: GridView.count(
                            childAspectRatio: ScreenUtil().setWidth(166) /
                                ScreenUtil().setHeight(120),
                            crossAxisCount: 2,
                            crossAxisSpacing:
                                ScreenUtil().setWidth(4).toDouble(),
                            mainAxisSpacing:
                                ScreenUtil().setHeight(10).toDouble(),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(4).toDouble(),
                            ),
                            children: cats
                                .map((Category e) => CategoryItem(e))
                                .toList())))
          ],
        ));
  }
}

class CategoryItem extends StatelessWidget {
  CategoryItem(this.category);

  Category category;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          width: ScreenUtil().setWidth(166),
          height: ScreenUtil().setHeight(120),
          // padding: EdgeInsets.all(8),
          child: Material(
              elevation: 0,
              shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(16),
                ),
              ),
              child: new ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  ),
                  child: Stack(children: <Widget>[
                    Positioned.fill(
                        child: new Container(
                            child: FadeInImage.assetNetwork(
                      image: category.image,
                      placeholder: "assets/images/placeholder.png",
                      fit: BoxFit.cover,
                    ))),
                    Positioned.fill(
                        child: Container(
                      height: ScreenUtil().setHeight(120),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          gradient: LinearGradient(
                              begin: FractionalOffset.topCenter,
                              end: FractionalOffset.bottomCenter,
                              colors: [
                                Colors.black.withOpacity(0.0),
                                Colors.black.withOpacity(0.6),
                              ],
                              stops: [
                                0.0,
                                1.0
                              ])),
                    )),
                    Positioned(
                        left: 0,
                        bottom: ScreenUtil().setHeight(10),
                        child: Container(
                            width: ScreenUtil().setWidth(166),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: ScreenUtil().setWidth(12),
                                ),
                                Text(
                                  category.name,
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(13.4),
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                Text(
                                  category.totalads.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: ScreenUtil().setSp(11.4),
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  width: ScreenUtil().setWidth(4),
                                ),
                              ],
                            )))
                  ])))),
      onTap: () {
        Navigator.push(context,
            new MaterialPageRoute(builder: (BuildContext context) {
          return ProductsByCategory(category);
        }));
      },
    );
  }
}

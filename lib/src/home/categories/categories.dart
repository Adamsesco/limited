import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/home/categories/products_by_category.dart';

class Categories_Horizontal extends StatefulWidget {
  Categories_Horizontal(this.categories);
  List<Category> categories;

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories_Horizontal> {


  @override
  Widget build(BuildContext context) {
    return Container(
        height: ScreenUtil().setHeight(126),
       /* padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(10)),*/
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: widget.categories
              .map((Category e) => CategoryWidget(e))
              .toList(),
        ));
  }
}

class CategoryWidget extends StatelessWidget {
  CategoryWidget(this.category);

  Category category;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: ScreenUtil().setWidth(70).toDouble(),
              height: ScreenUtil().setWidth(70).toDouble(),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(category.image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(4),
            ),
            Text(
              category.name,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(11.4),
                  fontWeight: FontWeight.w500),
            )
          ],
        )),onTap: (){
        Navigator.push(context,
            new MaterialPageRoute(builder: (BuildContext context) {
              return ProductsByCategory(category);
            }));

    },);
  }
}

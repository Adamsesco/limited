import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/items/product_item1.dart';
import 'package:limited/src/services/categories_services.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/settings/tabbar_custom.dart';
import 'package:limited/src/store/products_store_item.dart';

class ProductsByCategory extends StatefulWidget {
  ProductsByCategory(this.category);

  Category category;

  @override
  _ProductsByCategoryState createState() => _ProductsByCategoryState();
}

class _ProductsByCategoryState extends State<ProductsByCategory>
    with SingleTickerProviderStateMixin {
  List<Product> products = new List<Product>();
  ProductSServices prodserv = new ProductSServices();
  bool load = true;
  TabController _tabController;

  List<Category> cats = new List<Category>();
  CategoriesService cat = new CategoriesService();
  bool load_sub = true;

  getList() async {
    var a = await cat.get_sub_categories(widget.category.id);
    if (!this.mounted) return;
    setState(() {
      cats = List<Category>.from(a);
      cats.insert(0, Category(id: "-1", name: "All", check: false, image: ""));

      _tabController = new TabController(length: cats.length, vsync: this);
      load_sub = false;
    });
  } //load_sub

  get_product_by_category() async {

    setState(() {
      load = true;
    });
    List<Product> prods =
        await prodserv.get_product_by_category(widget.category.id);
    if (!this.mounted) return;
    setState(() {
      products = prods;
      load = false;
    });
  }

  get_product_by_sub_category(id_sub) async {

    setState(() {
      load = true;
    });
    List<Product> prods = await prodserv.get_product_by_sub_category(id_sub);
    if (!this.mounted) return;
    setState(() {
      products = prods;
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getList();
    get_product_by_category();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget list_sub = load_sub
        ? Center(
            child: CupertinoActivityIndicator(),
          )
        : Container(
            height: 70.h,
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(29)),
            child: ButtonsTabBar(
                onPressed: (i) {
                  if (i == 0) {
                    get_product_by_category();
                  } else {
                    get_product_by_sub_category(cats[i].id);
                  }
                },
                controller: _tabController,
                backgroundColor: Config.col_grey_d,
                radius: 24,
                unselectedBackgroundColor: const Color(0xffF1F0F5),
                unselectedLabelStyle: TextStyle(color: Colors.black),
                labelStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                tabs: cats
                    .map((e) => Tab(
                          text: e.name,
                        ))
                    .toList() /*[
          Tab(
            text: "ALL",
          ),
          Tab(
            text: "NORMAL",
          ),
          Tab(
            text: "AUCTIONS",
          ),
        ],*/
                ));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        appBar: LAppBar(
          true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(children: [
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  text(widget.category.name),
                  Container(
                    height: 2,
                  ),
                  text2(products.length.toString() + " Results"),
                ],
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                children: [
                  SvgPicture.asset("assets/images/icons/filter.svg"),
                  Container(
                    width: 4,
                  ),
                  Text(
                    "Filter",
                    style: TextStyle(
                        color: Config.col_grey_d,
                        fontSize: ScreenUtil().setSp(14),
                        fontWeight: FontWeight.w900),
                  ),
                  Container(
                    width: ScreenUtil().setWidth(28),
                  )
                ],
              )
            ],
          ),
          list_sub,
          load == true
              ? Center(
                  child: CupertinoActivityIndicator(),
                )
              : products.isEmpty
                  ? Center(
                      child: Text("No item found !"),
                    )
                  : Expanded(
                      child: GridView.count(
                      childAspectRatio: 166.w / 230.h,
                      crossAxisCount: 2,
                      // to disable GridView's scrolling
                      shrinkWrap: true,
                      // You won't see infinite size error
                      children:
                          products.map((e) => ProductStoreItem(e)).toList(),
                    ))
        ]));
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/widgets/favorite_widget.dart';
import 'package:limited/src/detail_product/details_product.dart';

class BestBidWidget extends StatelessWidget {
  BestBidWidget(this.bids);

  List<Product> bids;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(304),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: bids.map((Product e) => BestBidItem(e)).toList(),
      ),
    );
  }
}

class BestBidItem extends StatefulWidget {
  BestBidItem(this.product);

  Product product;

  @override
  _BestBidItemState createState() => _BestBidItemState();
}

class _BestBidItemState extends State<BestBidItem> {
  Duration remaining = DateTime.now().difference(DateTime.now());
  Timer t;
  int days = 0, hrs = 0, mins = 0;

  startTimer() async {
    t = Timer.periodic(Duration(seconds: 1), (timer) {
      if(!this.mounted) return;
      setState(() {
        remaining = widget.product.expired_date.difference(DateTime.now());
        mins = remaining.inMinutes;
        hrs = mins >= 60 ? mins ~/ 60 : 0;
        days = hrs >= 24 ? hrs ~/ 24 : 0;
        hrs = hrs % 24;
        mins = mins % 60;
      });
    });
  }

  TextStyle texts = TextStyle(
      fontSize: ScreenUtil().setSp(12.2),
      fontWeight: FontWeight.w300,
      color: Colors.white.withOpacity(0.7));

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.product.type.toString() == "1" &&
        widget.product.expired_date != null) {
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(widget.product);
          }));
        },
        child: Container(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(10),
                right: ScreenUtil().setWidth(10)),
            width: ScreenUtil().setWidth(216),
            height: ScreenUtil().setHeight(288),
            // padding: EdgeInsets.all(8),
            child: Material(
                elevation: 0,
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  ),
                ),
                child: new ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  ),
                  child: Stack(
                    children: <Widget>[
                      widget.product.images == null
                          ? Container()
                          : Positioned.fill(
                              child: new Container(
                                  child: FadeInImage.assetNetwork(
                              image: widget.product.images[0],
                              placeholder: "assets/images/placeholder.png",
                              fit: BoxFit.cover,
                            ))),
                      Positioned.fill(
                          child: Container(
                        height: ScreenUtil().setHeight(288),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                                begin: FractionalOffset.topCenter,
                                end: FractionalOffset.bottomCenter,
                                colors: [
                                  Colors.black.withOpacity(0.0),
                                  Colors.black.withOpacity(0.6),
                                ],
                                stops: [
                                  0.0,
                                  1.0
                                ])),
                      )),
                      Positioned(
                        bottom: ScreenUtil().setHeight(10),
                        left: 0,
                        child: new Container(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: [
                                Container(
                                  width: ScreenUtil().setWidth(24),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.product.name.toString(),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(12.5),
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white.withOpacity(0.8)),
                                    ),
                                    Container(
                                      height: 4,
                                    ),
                                    Text(
                                      widget.product.category.toString() ==
                                              "null"
                                          ? ""
                                          : widget.product.category.toString(),
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(11.2),
                                          fontWeight: FontWeight.w300,
                                          color: Colors.white.withOpacity(0.7)),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            Container(
                              height: ScreenUtil().setHeight(8),
                            ),
                            Container(
                                width: ScreenUtil().setWidth(180),
                                margin: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(8)),
                                padding:
                                    EdgeInsets.all(ScreenUtil().setWidth(8)),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(24),
                                  color: Colors.white.withOpacity(0.26),
                                ),
                                child: Row(children: [
                                  Text(
                                    "Last bid",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(11.2),
                                        fontWeight: FontWeight.w300,
                                        color: Colors.white.withOpacity(0.7)),
                                  ),
                                  Expanded(
                                    child: Container(),
                                  ),
                                  Text(
                                    widget.product.last_price.toString() ==
                                            "null"
                                        ? ""
                                        : widget.product.last_price.toString() +
                                            " KD",
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(13.2),
                                        fontWeight: FontWeight.w700,
                                        color: Colors.white),
                                  ),
                                ])),
                          ],
                        )),
                      ),
                      Positioned(
                          left: 0,
                          top: ScreenUtil().setHeight(4),
                          child: Container(
                              width: ScreenUtil().setWidth(194),
                              child: Row(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(
                                          left: ScreenUtil().setWidth(8)),
                                      padding: EdgeInsets.all(
                                          ScreenUtil().setWidth(8)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(24),
                                        color: Colors.white.withOpacity(0.26),
                                      ),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: ScreenUtil().setWidth(8),
                                          ),
                                          SvgPicture.asset(
                                              "assets/images/icons/timer.svg"),
                                          Container(
                                            width: ScreenUtil().setWidth(4),
                                          ),
                                          widget.product.expired_date == null
                                              ? Text(
                                                  "") /** widget.product.expired_date.toString()*/
                                              : Container(
                                               //   width: 180.w,
                                                  // height: 100,
                                                  child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                '$days d ',
                                                                style: texts,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                              Container(
                                                                width: 2.w,
                                                              ),
                                                              Text(
                                                                '$hrs h',
                                                                style: texts,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                              Container(
                                                                width: 4.w,
                                                              ),
                                                              Text(
                                                                '$mins m',
                                                                style: texts,
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                            ])
                                                        /* Text(
                                            product.time.toString(),
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize:
                                                    ScreenUtil().setSp(12.2),
                                                fontWeight: FontWeight.w300,
                                                color: Colors.white
                                                    .withOpacity(0.7)),
                                          ),*/
                                                      ])),
                                          Container(
                                            width: ScreenUtil().setWidth(0),
                                          ),
                                        ],
                                      )),
                                  Expanded(
                                    child: Container(),
                                  ),
                                  FavoriteButton(
                                    widget.product,
                                  ),
                                  Container(
                                    width: 4,
                                  )
                                ],
                              )))
                    ],
                  ),
                ))));
  }
}

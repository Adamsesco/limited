import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/store/products_store_item.dart';

class Product_products_All extends StatefulWidget {
  @override
  _Product_Best_Bids_AllState createState() => _Product_Best_Bids_AllState();
}

class _Product_Best_Bids_AllState extends State<Product_products_All> {

  List<Product> products = new List<Product>();
  ProductSServices prodserv = new ProductSServices();
  bool load = true;

  get_product_all() async {
    List<Product> prods =
    await prodserv.get_all_product();
    if (!this.mounted) return;
    setState(() {
      products = prods;
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_product_all();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: LAppBar(
          true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(children: [

          Container(height: 24.h,),
          load == true
              ? Center(
            child: CupertinoActivityIndicator(),
          )
              : products.isEmpty
              ? Center(
            child: Text("No item found !"),
          )
              : Expanded(
              child: GridView.count(
                childAspectRatio: 166.w / 230.h,
                crossAxisCount: 2,
                // to disable GridView's scrolling
                shrinkWrap: true,
                // You won't see infinite size error
                children:
                products.map((e) => ProductStoreItem(e)).toList(),
              ))
        ]));
  }
}

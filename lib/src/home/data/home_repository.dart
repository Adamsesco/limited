import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/homereponse.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/models/store.dart';

class HomeRepository {
  Future<HomeResponse> getInfoHome() async {
    final request = await Dio()
        .get<String>('http://134.122.118.107/limited/api/homescreen');

    if (request == null || request.statusCode != 200 || request.data == null) {
      return HomeResponse(
          responseCode: 0, message: null, categories: [], bids: [], stores: []);
    }

    final dynamic dataJson = jsonDecode(request.data);

    if (dataJson == null /*|| !(dataJson is Map<String, dynamic>)*/) {
      return HomeResponse(
          responseCode: 0,
          message: null,
          categories: [],
          bids: [],
          stores: [],
          products: []);
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;

    /*  if (code == null || message == null) {
      return HomeResponse(
        responseCode: 0,
        message: null,
          categories: [],
          bids: [],
          stores: []
      );
    }

 if (code != 1) {
      return HomeResponse(
        responseCode: code,
        message: message,
          categories: [],
          bids: [],
          stores: []
      );
    }*/

    final categories_List = dataJson["categories"] as List<dynamic>;
    final posts = categories_List.map((p) {
      final pst = p as Map<String, dynamic>;
      final post = Category.fromMap(pst);
      return post;
    }).toList();

    final store_List = dataJson["stores"] as List<dynamic>;
    final stores = store_List.map((p) {
      final pst = p as Map<String, dynamic>;
      final post = Store.fromMap(pst);
      return post;
    }).toList();

    var bids;
    List<Product> bids_list = [];

    if (dataJson.containsKey('bids')) {
      if (dataJson["bids"] == null) {
        bids_list = [];
      } else {
        bids = dataJson["bids"] as List<dynamic>;

        bids_list = List<Product>.from(bids.map((p) {
          final pst = p as Map<String, dynamic>;
          final post = Product.fromMap(pst);
          return post;
        }).toList());
      }
    } else {
      bids_list = [];
    }

    var fea;
    List<Product> fea_list = [];

    if (dataJson.containsKey('products')) {
      if (dataJson["products"] == null) {
        fea_list = [];
      } else {
        fea = dataJson["products"] as List<dynamic>;

        fea_list = List<Product>.from(fea.map((p) {
          final pst = p as Map<String, dynamic>;
          final post = Product.fromMap(pst);
          return post;
        }).toList());
      }
    } else {
      fea_list = [];
    }

    return HomeResponse(
        responseCode: code,
        message: message,
        categories: posts,
        stores: stores,
        bids: List<Product>.from(bids_list),
        products: List<Product>.from(fea_list));
  }
}

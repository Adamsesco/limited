import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/common/models/store.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:limited/src/store/products_store_item.dart';
import 'package:limited/src/store/store_details.dart';

class Stores_All extends StatefulWidget {
  @override
  _Product_Best_Bids_AllState createState() => _Product_Best_Bids_AllState();
}

class _Product_Best_Bids_AllState extends State<Stores_All> {
  List<Store> storees = new List<Store>();
  ProductSServices prodserv = new ProductSServices();
  bool load = true;

  get_stores_all() async {
    List<Store> prods = await prodserv.get_all_stores();
    if (!this.mounted) return;
    setState(() {
      storees = prods;
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_stores_all();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: LAppBar(
          true,
          leading_widget: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(children: [
          Container(
            height: 24.h,
          ),
          load == true
              ? Center(
                  child: CupertinoActivityIndicator(),
                )
              : storees.isEmpty
                  ? Center(
                      child: Text("No item found !"),
                    )
                  : Expanded(

              child: GridView.count(
                childAspectRatio: ScreenUtil().setWidth(166) /
                    ScreenUtil().setHeight(120),
                crossAxisCount: 2,
                crossAxisSpacing:
                ScreenUtil().setWidth(4).toDouble(),
                mainAxisSpacing:
                ScreenUtil().setHeight(10).toDouble(),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(4).toDouble(),
                ),
                children: storees.map((e) => StoreItem(e)).toList(),
                    ))
        ]));
  }
}

class StoreItem extends StatelessWidget {
  StoreItem(this.store);

  Store store;


  @override
  Widget build(BuildContext context) {


    return InkWell(
      child: Container(
          width: ScreenUtil().setWidth(166),
          height: ScreenUtil().setHeight(120),
          // padding: EdgeInsets.all(8),
          child: Material(
              elevation: 0,
              shape: new RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(16),
                ),
              ),
              child: new ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  ),
                  child: Stack(children: <Widget>[
                    Positioned.fill(
                        child: new Container(
                            child: FadeInImage.assetNetwork(
                              image: store.images[0],
                              placeholder: "assets/images/placeholder.png",
                              fit: BoxFit.cover,
                            ))),
                    Positioned.fill(
                        child: Container(
                          height: ScreenUtil().setHeight(120),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              gradient: LinearGradient(
                                  begin: FractionalOffset.topCenter,
                                  end: FractionalOffset.bottomCenter,
                                  colors: [
                                    Colors.black.withOpacity(0.0),
                                    Colors.black.withOpacity(0.6),
                                  ],
                                  stops: [
                                    0.0,
                                    1.0
                                  ])),
                        )),

                    Align(alignment: Alignment.center,child:
                    Container(
                        width: ScreenUtil().setWidth(70).toDouble(),
                        height: ScreenUtil().setWidth(70).toDouble(),
                        decoration: store.image == null
                            ? BoxDecoration()
                            : BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: NetworkImage(store.image),
                              fit: BoxFit.cover,
                            ))),
                      ),
                    Positioned(
                        left: 0,
                        bottom: ScreenUtil().setHeight(10),
                        child: Container(
                            width: ScreenUtil().setWidth(166),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: ScreenUtil().setWidth(12),
                                ),
                                Text(
                                  store.name,
                                  style: TextStyle(
                                      fontSize: ScreenUtil().setSp(13.4),
                                      color: Colors.white,
                                      fontFamily: "Roboto Condensed",

                                      fontWeight: FontWeight.w700),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                Text(
                                  store.id.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: ScreenUtil().setSp(11.4),
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  width: ScreenUtil().setWidth(4),
                                ),
                              ],
                            )))
                  ])))),
      onTap: () {
        Navigator.push(context,
            new MaterialPageRoute(builder: (BuildContext context) {
              return StoreDetails(store);
            }));
      },
    );
  }
}

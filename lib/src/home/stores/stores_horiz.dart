import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/models/store.dart';
import 'package:limited/src/store/store_details.dart';

class Stores_Horizontal extends StatefulWidget {
  Stores_Horizontal(this.stores);

  List<Store> stores;

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Stores_Horizontal> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: ScreenUtil().setHeight(120),
        /* padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(10), right: ScreenUtil().setWidth(10)),*/
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: widget.stores.map((Store e) => StoreWidget(e)).toList(),
        ));
  }
}

class StoreWidget extends StatelessWidget {
  StoreWidget(this.store);

  Store store;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: (){
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
                return StoreDetails(store);
              }));
        },
        child: Container(
        padding: EdgeInsets.only(left: ScreenUtil().setWidth(13)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: ScreenUtil().setWidth(70).toDouble(),
                height: ScreenUtil().setWidth(70).toDouble(),
                decoration: store.image == null
                    ? BoxDecoration()
                    : BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: NetworkImage(store.image),
                          fit: BoxFit.cover,
                        ))),
            SizedBox(
              height: ScreenUtil().setHeight(4),
            ),
            Text(
              store.name,
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(11.4),
                  fontWeight: FontWeight.w500),
            )
          ],
        )));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/detail_product/details_product.dart';

class FeaturedItems extends StatefulWidget {
  FeaturedItems(this.products);

  List<Product> products;

  @override
  _FeaturedItemsState createState() => _FeaturedItemsState();
}

class _FeaturedItemsState extends State<FeaturedItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: ScreenUtil().setHeight(210),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children:
              widget.products.map((Product e) => FeaturedItem(e)).toList(),
        ));
  }
}

class FeaturedItem extends StatelessWidget {
  FeaturedItem(this.product);

  Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(10)),
            height: ScreenUtil().setHeight(198),
            width: ScreenUtil().setWidth(130),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                    //<--clipping image

                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/placeholder.png"),
                                fit: BoxFit.contain)),
                        child: product.image == null
                            ? Container()
                            : Hero(
                                tag: product.image,
                                child: FadeInImage.assetNetwork(
                                  image: product.image[0],
                                  placeholder: "assets/images/placeholder.png",
                                  width: ScreenUtil().setWidth(130),
                                  height: ScreenUtil().setHeight(108),
                                  fit: BoxFit.cover,
                                )))),

                SizedBox(
                  height: 8,
                ),

                Container(
                   // height: ScreenUtil().setHeight(54),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.name.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                SizedBox(
                  height: 4,
                ),
                Container(
                    height: ScreenUtil().setHeight(34),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.description.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                // Expanded(child: Container(),),
                Container(
                  child: Text(
                "KD " + product.last_price.toString() ,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: ScreenUtil().setSp(17),
                    ),
                  ),
                ),
              ],
            )),
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(product);
          }));
        });
  }
}

import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/product.dart';
import 'package:limited/src/items/product_item1.dart';
import 'package:limited/src/services/products_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SavedItems extends StatefulWidget {
  @override
  _SavedItemsState createState() => _SavedItemsState();
}

class _SavedItemsState extends State<SavedItems> {
  List<Product> products = new List<Product>();
  ProductSServices prodserv = new ProductSServices();
  bool load = true;
  String token;
  List<Product> products_fixed = new List<Product>();
  List<Product> products_auctions = new List<Product>();

  //type

  get_product_by_fav() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString("login_token");
    List<Product> prods = await prodserv.get_product_by_favorites(token);
    if (!this.mounted) return;
    setState(() {
      products = prods;
      products_auctions =
          products.where((Product element) => element.type == "1").toList();
      products_fixed =
          products.where((Product element) => element.type == "0").toList();

      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_product_by_fav();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(23),
          bottom: ScreenUtil().setHeight(23),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    return DefaultTabController(
        length: 3,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          text("Saved items"),
          Container(
            height: 2,
          ),
          Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(29)),
              child: ButtonsTabBar(
                backgroundColor: Config.col_grey_d,
                radius: 24,
                unselectedBackgroundColor: const Color(0xffF1F0F5),
                unselectedLabelStyle: TextStyle(color: Colors.black),
                labelStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                tabs: [
                  Tab(
                    text: "ALL",
                  ),
                  Tab(
                    text: "NORMAL",
                  ),
                  Tab(
                    text: "AUCTIONS",
                  ),
                ],
              )),
          Expanded(
            child: TabBarView(children: [
              load == true
                  ? Center(
                      child: CupertinoActivityIndicator(),
                    )
                  : products.isEmpty
                      ? Center(
                          child: Text("No item found !"),
                        )
                      : ListView(
                          children:
                              products.map((e) => ProductItem(e)).toList(),
                        ),
              load == true
                  ? Center(
                      child: CupertinoActivityIndicator(),
                    )
                  : products_fixed.isEmpty
                      ? Center(
                          child: Text("No item found !"),
                        )
                      : ListView(
                          children: products_fixed
                              .map((e) => ProductItem(e))
                              .toList(),
                        ),
              load == true
                  ? Center(
                      child: CupertinoActivityIndicator(),
                    )
                  : products_auctions.isEmpty
                      ? Center(
                          child: Text("No item found !"),
                        )
                      : ListView(
                          children: products_auctions
                              .map((e) => ProductItem(e))
                              .toList(),
                        ),
            ]),
          )
        ]));
  }
}

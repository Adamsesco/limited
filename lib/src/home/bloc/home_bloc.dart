import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:limited/src/home/bloc/home_event.dart';
import 'package:limited/src/home/bloc/home_state.dart';
import 'package:limited/src/home/data/home_repository.dart';
import 'package:meta/meta.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeRepository homeRepository;

  HomeBloc({@required this.homeRepository});

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is Homeequested) {
      yield FeedLoadInProgress();
      final feedResponse = await homeRepository.getInfoHome();
      if (feedResponse != null /*&& feedResponse.responseCode == 1*/) {
        yield FeedLoadSuccess(homeResponse: feedResponse);
      } else {
        yield FeedLoadFailure(homeResponse: feedResponse);
      }
    }
  }

  @override
  // TODO: implement initialState
  HomeState get initialState => InitialFeedState();
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/home/Featured_items/featured_items.dart';
import 'package:limited/src/home/bloc/bloc.dart';
import 'package:limited/src/home/categories/categories.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:limited/src/home/products_horiz/gete_all_product.dart';
import 'package:limited/src/home/products_horiz/product_best_bid_widget.dart';
import 'package:limited/src/home/products_horiz/product_best_bids_all.dart';
import 'package:limited/src/home/stores/all_stores.dart';
import 'package:limited/src/home/stores/stores_horiz.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.add(Homeequested());
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(29),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.w900),
        ));

    return BlocBuilder<HomeBloc, HomeState>(builder: (context, homeState) {
      if (homeState is FeedLoadInProgress) {
        return Center(
          child: CupertinoActivityIndicator(),
        );
      } else if (homeState is FeedLoadSuccess) {
        final categories = homeState.homeResponse.categories;
        final bids = homeState.homeResponse.bids;
        final stores = homeState.homeResponse.stores;
        final fea_items = homeState.homeResponse.products;

        /* if (posts.isEmpty) {
    return _EmptyFeed();
    }*/

        return ListView(
          children: [
            text("categories".tr()),
            Categories_Horizontal(categories),
            Container(
              height: ScreenUtil().setHeight(14),
            ),
            Container(
                child: Row(children: [
              text("Best Bids"),
              Expanded(
                child: Container(),
              ),
              FlatButton(
                  onPressed: () {

                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                          return Product_Best_Bids_All();
                        }));
                  },
                  child: Text(
                    "See more ..",
                    style: TextStyle(color: Config.col_blue,
                    fontWeight: FontWeight.bold
                    ),
                  ))
            ])),
            BestBidWidget(bids),
            Container(
              height: ScreenUtil().setHeight(10),
            ),
            Container(
                child: Row(children: [
                  text("Stores"),
                  Expanded(
                    child: Container(),
                  ),
                  FlatButton(
                      onPressed: () {

                        Navigator.push(context,
                            new MaterialPageRoute(builder: (BuildContext context) {
                              return Stores_All();
                            }));
                      },
                      child: Text(
                        "See more ..",
                        style: TextStyle(color: Config.col_blue,
                            fontWeight: FontWeight.bold
                        ),
                      ))
                ])),
            Stores_Horizontal(stores),
            Container(
              height: ScreenUtil().setHeight(10),
            ),


            Container(
                child: Row(children: [
                  text("Featured items"),
                  Expanded(
                    child: Container(),
                  ),
                  FlatButton(
                      onPressed: () {

                        Navigator.push(context,
                            new MaterialPageRoute(builder: (BuildContext context) {
                              return Product_products_All();
                            }));
                      },
                      child: Text(
                        "See more ..",
                        style: TextStyle(color: Config.col_blue,
                            fontWeight: FontWeight.bold
                        ),
                      ))
                ])),
            FeaturedItems(fea_items)
          ],
        );
      } else {
        return Container();
      }
    });
  }
}

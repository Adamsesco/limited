import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/publish/publish3.dart';
import 'package:limited/src/services/categories_services.dart';

class Publish2 extends StatefulWidget {
  Publish2(this.json);

  var json = {};

  @override
  _Publish2State createState() => _Publish2State();
}

class _Publish2State extends State<Publish2> {
  List<Category> cats = new List<Category>();
  CategoriesService cat = new CategoriesService();
  bool load = true;

  getList() async {
    var a = await cat.get_sub_categories(widget.json["category"].id);
    if (!this.mounted) return;
    setState(() {
      cats = List<Category>.from(a);
      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  Category choice_category;

  click_sub_cat(Category category) {
    if (category.check == false) {
      setState(() {
        for (Category ct in cats) {
          setState(() {
            ct.check = false;
          });
        }
        category.check = true;
        choice_category = category;
      });
    } else
      setState(() {
        category.check = false;
        choice_category = null;
      });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4(Category category) => TextStyle(
        color: category.check ? Config.col_blue : Config.col_grey_d,
        fontSize: ScreenUtil().setSp(category.check ? 16 : 14),
        fontWeight: category.check ? FontWeight.w900 : FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(Category category, onPressed) => InkWell(
          child: Column(children: [
            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(0),
                  bottom: ScreenUtil().setHeight(0),
                  left: ScreenUtil().setWidth(23),
                  right: ScreenUtil().setWidth(23),
                ),
                child: Row(children: [
                  Expanded(
                      child: Text(
                    category.name,
                    style: text4(category),
                  )),
                  new IconButton(
                    padding: EdgeInsets.all(0.0),
                    onPressed: () {
                      //widget.func();

                      onPressed(category);
                    },
                    icon: new Container(
                        margin: new EdgeInsets.only(left: 0.0, right: 0.0),
                        child: Container(
                          child: Container(
                            width: 10,
                            height: 10,
                          ),
                          decoration: new BoxDecoration(
                            color: category.check
                                ? Colors.white
                                : Colors.grey[200],
                            border: new Border.all(
                                width: 5.0,
                                color: category.check
                                    ? Config.col_blue
                                    : Colors.grey[200]),
                            borderRadius: const BorderRadius.all(
                                const Radius.circular(36.0)),
                          ),
                        )),
                  )
                ])),
            divid
          ]),
          onTap: () {
            onPressed(category);
          },
        );
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w500),
        ));

    return Scaffold(
      appBar: LAppBar(
        true,
        leading_widget: IconButton(
          icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(children: [
        text("Sell your items"),
        Container(
          height: 2,
        ),
        text2("Select a sub Category"),
        load == true
            ? Center(
                child: CupertinoActivityIndicator(),
              )
            : cats.isEmpty
                ? Center(
                    child: Text("No item found !"),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children:
                        cats.map((e) => row_widget(e, click_sub_cat)).toList(),
                  ),
        Container(
          height: ScreenUtil().setHeight(52),
        ),
        Column(children: [
          Container(
              height: ScreenUtil().setHeight(46),
              width: ScreenUtil().setWidth(135),
              child: RaisedButton(
                color: choice_category == null
                    ? Color(0xffF1F0F5)
                    : Config.col_blue,
                elevation: 0,
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(28),
                  ),
                ),
                onPressed: () {
                //  if (choice_category != null) {
                    widget.json["sub_category"] = choice_category;

                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return Publish3(widget.json);
                    }));
                 // }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "NEXT",
                      style: TextStyle(
                          color: choice_category == null
                              ? Color(0xffcccccc)
                              : Colors.white),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(36),
                    ),
                    SvgPicture.asset("assets/images/icons/arrow_next.svg",
                        color: choice_category == null
                            ? Color(0xffcccccc)
                            : Colors.white)
                  ],
                ),
              )),
        ])
      ]),
    );
  }
}

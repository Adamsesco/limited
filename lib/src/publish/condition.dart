import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';

class Condition extends StatefulWidget {
  Condition(this.json);

  var json;

  @override
  _ConditionState createState() => _ConditionState();
}

class _ConditionState extends State<Condition> {
  List cats = [
    {"name": "New", "check": false, "id": "1"},
    {"name": "Used", "check": false, "id": "2"},
    {"name": "Antique", "check": false, "id": "3"},
  ].map((e) => Category.fromMap(e)).toList();

  Category choice_category;

  click_sub_cat(Category category) {
    if (category.check == false) {
      setState(() {
        for (Category ct in cats) {
          setState(() {
            ct.check = false;
          });
        }
        category.check = true;
        choice_category = category;
      });
    } else
      setState(() {
        category.check = false;
        choice_category = null;
      });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text4(Category category) => TextStyle(
        color: category.check ? Config.col_blue : Config.col_grey_d,
        fontSize: ScreenUtil().setSp(category.check ? 16 : 14),
        fontWeight: category.check ? FontWeight.w900 : FontWeight.w500);

    Widget row_widget(Category category, onPressed) => InkWell(
          child: Column(children: [
            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(0),
                  bottom: ScreenUtil().setHeight(0),
                  left: ScreenUtil().setWidth(23),
                  right: ScreenUtil().setWidth(23),
                ),
                child: Row(children: [
                  Expanded(
                      child: Text(
                    category.name,
                    style: text4(category),
                  )),
                  new IconButton(
                    padding: EdgeInsets.all(0.0),
                    onPressed: () {
                      //widget.func();

                      onPressed(category);
                    },
                    icon: new Container(
                        margin: new EdgeInsets.only(left: 0.0, right: 0.0),
                        child: Container(
                          child: Container(
                              width: 15.w,
                              height: 15.w,
                              child: category.check == true
                                  ? Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 14.w,
                                    )
                                  : Container()),
                          decoration: new BoxDecoration(
                            color: category.check
                                ? Config.col_blue
                                : Colors.grey[200],

                            /*  border: new Border.all(
                            width: 5.0,
                            color: category.check
                                ? Config.col_blue
                                : Colors.grey[200]),*/
                            borderRadius: const BorderRadius.all(
                                const Radius.circular(2.0)),
                          ),
                        )),
                  )
                ])),
          ]),
          onTap: () {
            onPressed(category);
          },
        );
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w500),
        ));

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            Container(
                child: FlatButton(
              onPressed: () {
                if (choice_category != null) {
                  widget.json["condition"] = choice_category.name;

                  Navigator.pop(context,widget.json["condition"]);
                  /* Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                          return Publish3(widget.json);
                        }));*/
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Done",
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 15.5.sp,
                        color: choice_category == null
                            ? Color(0xffcccccc)
                            : Config.col_blue),
                  ),
                ],
              ),
            )),
          ]),
      body: ListView(children: [
        Container(
          height: 12.h,
        ),
        text("Condition"),
        Container(
          height: 2,
        ),
        text2("Lorem ipsum dolor sit amet, consectetur adipiscing."),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: cats.map((e) => row_widget(e, click_sub_cat)).toList(),
        ),
        Container(
          height: ScreenUtil().setHeight(52),
        ),
      ]),
    );
  }
}

import 'dart:io';

import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/publish/publish4.dart';

class Publish3 extends StatefulWidget {
  Publish3(this.json);

  var json;

  @override
  _Publish3State createState() => _Publish3State();
}

class _Publish3State extends State<Publish3> {
  var titleCtrl = TextEditingController();
  var subCtrl = TextEditingController();
  var description = TextEditingController();
  String path = "";

  //List<String> list_images = [];
  List<String> list_images = ["", "", "", "", "", ""];

  @override
  void initState() {
    super.initState();

    print(widget.json);
  }

  Future compress_image(File image, index) async {
    File compressedFile =
        (await FlutterNativeImage.compressImage(image.path, quality: 70))
            as File;
    await save_image(compressedFile, index, "image");
    setState(() {
      lst_bl = false;
    });
  }

  save_image(image, int index, String type) async {
    int timestamp = new DateTime.now().millisecondsSinceEpoch;

    if (type == "image") {
      path = "post/img_" + timestamp.toString() + ".jpg";
    } else {
      path = "post/img_" + timestamp.toString() + ".mp4";
    }
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(path);
    await storageReference.put(image).onComplete.then((val) {
      val.ref.getDownloadURL().then((val) {
        //if (!mounted) return;

        print(val.toString());

        if (this.mounted) {
          setState(() {
            list_images.insert(index, val.toString());
          });
        }
      });
    });
  }

  /**


      pick_video() async {
      await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
      title: Text('Upload Video'),
      content: Text(''),
      actions: <Widget>[
      CupertinoDialogAction(
      onPressed: () async {
      Navigator.of(context).pop();
      File image =
      await ImagePicker.pickVideo(source: ImageSource.gallery);
      _onAddVideoPressed(image);
      },
      child: Text('Gallery'),
      ),
      CupertinoDialogAction(
      onPressed: () async {
      Navigator.of(context).pop();
      File video =
      await ImagePicker.pickVideo(source: ImageSource.camera);
      _onAddVideoPressed(video);
      },
      child: Text('Record video'),
      ),
      ],
      ),
      );
      }
   */

  bool lst_bl = false;

  pick_image(index) async {
    await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        content: Column(
          children: [
            Material(
                color: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                )),
            Text(
              "Publish :",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.sp),
            ),
            Container(
              height: 24,
            ),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();

                setState(() {
                  lst_bl = true;
                });

                var image =
                    await ImagePicker.platform.pickImage(source: ImageSource.gallery);
                print('----------------------------------------------');
                print(image);
                compress_image(   File(image.path)
                    , index);
              },
              child: Text('Photo Gallery'),
            ),
            Divider(),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();
                var image =
                    (await ImagePicker.platform.pickImage(source: ImageSource.camera))
                        ;
                compress_image(  File(image.path), index);
              },
              child: Text('Camera'),
            ),
            Divider(),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();
                var image =
                    await ImagePicker.platform.pickVideo(source: ImageSource.gallery);
                save_image(  File(image.path), index, "video");
              },
              child: Text('Video Gallery'),
            ),
            Divider(),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();
                var video =
                    await ImagePicker.platform.pickVideo(source: ImageSource.camera);
                save_image(  File(video.path), index, "video");
              },
              child: Text('Record video'),
            ),
          ],
        ),
        actions: <Widget>[

        ],
      ),
    );
  }

  Widget shpe(index) => InkWell(
      onTap: () {
        pick_image(index);
      },
      child: Container(
        decoration: DottedDecoration(
            shape: Shape.box,
            borderRadius: BorderRadius.circular(10),
            color: Config.col_grey_medium //remove this to get plane rectange
            ),
        width: ScreenUtil().setWidth(100),
        height: ScreenUtil().setWidth(100),
        child: list_images[index] == ""
            ? Center(
                child: SvgPicture.asset("assets/images/icons/add-photo.svg"),
              )
            : Stack(children: [

              Positioned.fill(child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                child: Image.network(
                  list_images[index],
                  fit: BoxFit.cover,
                  width: ScreenUtil().setWidth(100),
                  height: ScreenUtil().setWidth(100),
                ))),
          new Positioned(
              top: 0.0,
              right: 0.0,
              child: new InkWell(
                child: new Center(
                    child: new SvgPicture.asset(
                      "assets/images/remove.svg",
                    )),
                onTap: () {
                  setState(() {
                    list_images.remove(list_images[index]);
                  });
                },
              ))


  ])
      ));

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text_photo(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(18),
              fontWeight: FontWeight.w900),
        ));

    Widget text2_photo(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(14.5),
              fontWeight: FontWeight.w300),
        ));
    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    Widget textfield(String name, TextEditingController texted, max) =>
        TextFormField(
          decoration: InputDecoration(
            filled: true,
            fillColor: const Color(0xffFAFAFA),
            focusColor: const Color(0xffFAFAFA),
            hintStyle: TextStyle(color: const Color(0xffAFB3BE)),
            hintText: name,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: const Color(0xffFAFAFA),
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                  width: 2.0,
                )),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(28),
            ),
            // labelText: name,
            contentPadding:
                EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
          ),
          textAlignVertical: TextAlignVertical.top,
          maxLines: max,
          controller: texted,
          validator: (value) {
            if (value.isEmpty) return 'Value is empty';
            return null;
          },
        );

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            children: [
              text("Sell your items"),
              Container(
                height: 2,
              ),
              text2("Fill in the item details."),
              textfield("Title", titleCtrl, 1),
              Container(
                height: ScreenUtil().setHeight(10),
              ),
              textfield("SubTitle", subCtrl, 1),
              Container(
                height: ScreenUtil().setHeight(8),
              ),
              textfield("Description", description, 5),
              text_photo("Additional photos"),
              text2_photo(
                  "Add up to 6 photos, the first one will be the main."),
              (lst_bl == true)
                  ? Center(child: CupertinoActivityIndicator())
                  : Container(),
              Container(
                height: 6.h,
              ),
              Container(
                  height: ScreenUtil().setHeight(220),
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: ScreenUtil().setWidth(8),
                          right: ScreenUtil().setWidth(8)),
                      child: GridView.count(
                          childAspectRatio: ScreenUtil().setWidth(100) /
                              ScreenUtil().setHeight(100),
                          crossAxisCount: 3,
                          physics: NeverScrollableScrollPhysics(),
                          crossAxisSpacing: ScreenUtil().setWidth(4).toDouble(),
                          mainAxisSpacing:
                              ScreenUtil().setHeight(10).toDouble(),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(4).toDouble(),
                          ),
                          children: list_images
                              .map((String e) => shpe(list_images.indexOf(e)))
                              .toList()))),
              Column(children: [
                Container(height: 12,),

                Container(
                    height: ScreenUtil().setHeight(46),
                    width: ScreenUtil().setWidth(135),
                    child: RaisedButton(
                      color: Config.col_blue,
                      elevation: 0,
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(28),
                        ),
                      ),
                      onPressed: () {
                        widget.json['title'] = titleCtrl.text;
                        widget.json['subtitle'] = subCtrl.text;
                        widget.json['description'] = description.text;
                        widget.json["images"] = list_images;

                        Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) {
                          return Publish4(widget.json);
                        }));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "NEXT",
                            style: TextStyle(color: Colors.white),
                          ),
                          Container(
                            width: ScreenUtil().setWidth(36),
                          ),
                          SvgPicture.asset("assets/images/icons/arrow_next.svg",color: Colors.white)
                        ],
                      ),
                    )),
                Container(height: 12,),
              ])
            ]));
  }
}

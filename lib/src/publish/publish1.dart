import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/category.dart';
import 'package:limited/src/publish/publish2.dart';
import 'package:limited/src/services/categories_services.dart';

class Publish1 extends StatefulWidget {
  @override
  _Publish1State createState() => _Publish1State();
}

class _Publish1State extends State<Publish1> {
  List<Category> cats = new List<Category>();
  CategoriesService cat = new CategoriesService();
  bool load = true;
  var json = {};

  getList() async {
    var a = await cat.get_all_categories();
    if (!this.mounted) return;
    setState(() {
      cats = a;
      load = false;
    });
  }

  Category choice_category;

  click_cat(Category category) {
    /* setState(() {
      choice_category = null;
      for (Category ct in cats) {
        setState(() {
          ct.check = false;
        });
      }
    });*/
    if (category.check == false) {
      setState(() {
        for (Category ct in cats) {
          setState(() {
            ct.check = false;
          });
        }
        category.check = true;
        choice_category = category;
      });
    } else
      setState(() {
        category.check = false;
        choice_category = null;
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );
    Widget row_widget(Category e, onPressed) => Container(
        color:
            e.check == true ? Config.col_blue.withOpacity(0.2) : Colors.white,
        child: InkWell(
          child: Column(children: [
            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(11),
                  bottom: ScreenUtil().setHeight(11),
                  left: ScreenUtil().setWidth(23),
                  right: ScreenUtil().setWidth(23),
                ),
                child: Row(children: [
                  Image.network(e.image,
                      width: ScreenUtil().setWidth(22),
                      height: ScreenUtil().setWidth(22),
                      fit: BoxFit.cover),
                  Container(
                    width: ScreenUtil().setWidth(12),
                  ),
                  Expanded(
                      child: Text(
                    e.name,
                    style: text4,
                  )),
                  SvgPicture.asset("assets/images/icons/arrow.svg"),
                ])),
            divid
          ]),
          onTap: () {
            onPressed(e);
          },
        ));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w400),
        ));

    return load == true
        ? Center(
            child: CupertinoActivityIndicator(),
          )
        : ListView(children: [
            text("Sell your items"),
            Container(
              height: 2,
            ),
            text2("Select a Category"),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: cats.map((e) => row_widget(e, click_cat)).toList(),
            ),
            Container(
              height: ScreenUtil().setHeight(52),
            ),
            Column(children: [
              Container(
                  height: ScreenUtil().setHeight(46),
                  width: ScreenUtil().setWidth(135),
                  child: RaisedButton(
                    color: choice_category == null
                        ? Color(0xffF1F0F5)
                        : Config.col_blue,
                    elevation: 0,
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(28),
                      ),
                    ),
                    onPressed: () {
                      if(choice_category != null) {
                        json["category"]=choice_category;

                        Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) {
                              return Publish2(json);
                            }));
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "NEXT",
                          style: TextStyle(
                              color: choice_category == null
                                  ? Color(0xffcccccc)
                                  : Colors.white),
                        ),
                        Container(
                          width: ScreenUtil().setWidth(36),
                        ),
                        SvgPicture.asset("assets/images/icons/arrow_next.svg",
                            color: choice_category == null
                                ? Color(0xffcccccc)
                                : Colors.white)
                      ],
                    ),
                  )),
            ])
          ]);
  }
}

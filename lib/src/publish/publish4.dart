import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_toggle_tab/flutter_toggle_tab.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/custom_appbar.dart';
import 'package:limited/src/common/widgets/toggle_bar.dart';
import 'package:limited/src/main_screen.dart';
import 'package:limited/src/publish/condition.dart';
import 'package:limited/src/publish/duration.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Publish4 extends StatefulWidget {
  Publish4(this.json);

  var json;

  bool swit = true;

  @override
  _Publish4State createState() => _Publish4State();
}

class _Publish4State extends State<Publish4> {
  bool fixed = true;
  String condition = "";
  String duration = "";
  var _startprice = TextEditingController();
  var _price = TextEditingController();
  bool swit = true;
  String _selectedDate = '';
  RestService rest = RestService();
  bool show = false;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime d = await showDatePicker(
      initialDate: DateTime.now(),
      context: context,
      firstDate: DateTime(2015),
      lastDate: DateTime(2022),
    );
    if (d != null)
      setState(() {
        _selectedDate = new DateFormat.yMMMMd("en_US").format(d);
      });
  }

  @override
  Widget build(BuildContext context) {
    go_condition() async {
      String du = await Navigator.push(context,
          new MaterialPageRoute(builder: (BuildContext context) {
        return Condition(widget.json);
      }));

      setState(() {
        condition = du;
      });
    }

    gt(val) {
      setState(() {
        swit = val;
      });
    }

    go_duration() async {
      var dur = await Navigator.push(context,
          new MaterialPageRoute(builder: (BuildContext context) {
        return Duration(widget.json);
      }));

      setState(() {
        duration = dur;
      });
    }

    ScreenUtil.init(context, width: 375, height: 812);
    TextStyle text4 = TextStyle(
        color: Config.col_grey_d,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    TextStyle text_btn1 = TextStyle(
        color: Config.col_blue,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: Config.col_unselected.withOpacity(0.4),
    );

    Widget row_widget(String text, onPressed, {show}) => InkWell(
        onTap: () {
          if (text == "Scheduled for")
            _selectDate(context);
          else
            onPressed();
        },
        child: Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
              bottom: ScreenUtil().setHeight(10),
              left: ScreenUtil().setWidth(13),
              right: ScreenUtil().setWidth(12),
            ),
            child: Row(children: [
              Expanded(
                  child: Text(
                text,
                style: TextStyle(
                    color: (show == true && fixed == true)
                        ? const Color(0xffCCCCCC)
                        : Config.col_grey_d,
                    fontSize: ScreenUtil().setSp(14),
                    fontWeight: FontWeight.w500),
              )),
              Text((condition != "" &&
                      condition.toString() != "null" &&
                      text == "Condition")
                  ? condition
                  : ""),
              Text((duration != "" &&
                      duration.toString() != "null" &&
                      text == "Duration")
                  ? duration
                  : ""),
              Text((_selectedDate != "" &&
                      _selectedDate.toString() != "null" &&
                      text == "Scheduled for")
                  ? _selectedDate
                  : ""),
              Container(
                width: 8,
              ),
              SvgPicture.asset("assets/images/icons/arrow.svg",
                  color: (show == true && fixed == true)
                      ? const Color(0xffCCCCCC)
                      : Config.col_grey_d),
            ])));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: Config.col_grey_d,
              fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: LAppBar(true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: ListView(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            children: [
              text("Selling options"),
              Container(
                height: 2,
              ),
              text2("Lorem ipsum dolor sit amet, consectetur adipiscing."),
              /* FlutterToggleTab(
                // width in percent, to set full width just set to 100
                //  width: ScreenUtil().setWidth(103),
                borderRadius: 30,
                initialIndex: 0,
                height: ScreenUtil().setHeight(52),

                unSelectedBackgroundColors: [Color(0xffF1F0F5)],
                // initialLabelIndex: 0,
                selectedBackgroundColors: [Colors.white],
                selectedTextStyle: TextStyle(
                    color: Config.col_blue,
                    fontSize: ScreenUtil().setSp(16),
                    fontWeight: FontWeight.w800),
                unSelectedTextStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.w500),
                labels: ["Fixed Price", "Auction"],
                selectedLabelIndex: (index) {},
              ),*/

              ToggleBar(
                  labels: ["Fixed Price", "Auction"],
                  onSelectionUpdated: (index) => {
                        if (index == 0)
                          {setState(() => fixed = true)}
                        else
                          setState(() => fixed = false)

                        // Do something with index
                      }),
              Container(
                height: ScreenUtil().setHeight(32),
              ),
              divid,
              (fixed == true)
                  ? AbsorbPointer(
                      child: TextFormField(
                          cursorColor: Colors.black,
                          controller: _startprice,
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: (fixed == true)
                                      ? const Color(0xffCCCCCC)
                                      : Config.col_grey_d,
                                  fontSize: ScreenUtil().setSp(14)),
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(left: 16.w, right: 16.w),
                              hintText: "Starting price")))
                  : TextFormField(
                      cursorColor: Colors.black,
                      controller: _startprice,
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintStyle: TextStyle(
                              color: (fixed == true)
                                  ? const Color(0xffCCCCCC)
                                  : Config.col_grey_d,
                              fontSize: ScreenUtil().setSp(14)),
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding:
                              EdgeInsets.only(left: 16.w, right: 16.w),
                          hintText: "Starting price"),
                    ),
              divid,
              fixed == true
                  ? TextFormField(
                      controller: _price,
                      keyboardType: TextInputType.number,
                      cursorColor: Colors.black,
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintStyle: TextStyle(
                              color: (fixed == false)
                                  ? const Color(0xffCCCCCC)
                                  : Config.col_grey_d,
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w500),
                          contentPadding: EdgeInsets.only(
                              left: 16.w, right: 16.w, top: 0.h, bottom: 0.h),
                          hintText: "Price"),
                    )
                  : Container(),
              divid,
              row_widget("Duration", fixed == true ? (){} : go_duration,
                  show: true),
              divid,
              row_widget("Condition", go_condition),
              divid,
              row_widget("Scheduled for", _selectDate),
              divid,
              Container(
                height: ScreenUtil().setHeight(36),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "SPECIAL OFFER ",
                    style: text_btn1,
                  ),
                  Container(
                    width: ScreenUtil().setWidth(16),
                  ),
                  CupertinoSwitch(
                      activeColor: Config.col_blue,
                      value: widget.swit,
                      onChanged: (val) {
                        gt(val);
                      }),
                ],
              ),
              Container(
                height: ScreenUtil().setHeight(23),
              ),
              Center(
                  child: Container(
                width: ScreenUtil().setWidth(260),
                child: Text(
                  "If your offer is special or rare, please describe what distinguishes this offer.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Config.col_grey_d,
                      fontFamily: "Roboto light",
                      fontSize: ScreenUtil().setSp(11.5),
                      fontWeight: FontWeight.w300),
                ),
              )),
              Container(
                height: ScreenUtil().setHeight(35),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: ScreenUtil().setWidth(168),
                      height: ScreenUtil().setHeight(46),
                      child: RaisedButton(
                          color: Config.col_blue,
                          elevation: 1,
                          shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(28),
                            ),
                          ),
                          onPressed: () async {
                            setState(() {
                              show = true;
                            });
                            for (String i
                                in List<String>.from(widget.json["images"])) {
                              if (i == "") {
                                widget.json["images"].remove(i);
                              }
                            }

                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            String id = prefs.getString("id");
                            var js = {
                              "title": widget.json["title"],
                              "description": widget.json["description"],
                              "subtitle": widget.json["subtitle"],
                              "user_id": id.toString(),
                              "category_id":
                                  widget.json["category"].id.toString(),
                              "sub_category_id":
                                  widget.json["sub_category"].id.toString(),
                              "type": fixed == true ? 0 : 1,
                              "starting_price": _startprice.text == ""
                                  ? _price.text
                                  : _startprice.text,
                              "price": _price.text,
                              "duration": duration,
                              "is_special_offer": swit == true ? "1" : false,
                              "images": widget.json["images"],
                              "scheduled": _selectedDate,
                            };

                            var a =
                                await rest.post("addNewPost", json.encode(js));

                            print("------------------------");
                            print(a);
                            setState(() {
                              show = false;
                            });
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MainScreen()),
                                (Route<dynamic> route) => false);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? CupertinoTheme(
                                      data: CupertinoTheme.of(context).copyWith(
                                          brightness: Brightness.dark),
                                      child: CupertinoActivityIndicator())
                                  : Container(),
                              show //CupertinoActivityIndicator()
                                  ? Container(
                                      width: 4,
                                    )
                                  : Container(),
                              Text(
                                "CONFIRM",
                                style: text_btn2,
                              ),
                            ],
                          ))),
                ],
              )
            ]));
  }
}

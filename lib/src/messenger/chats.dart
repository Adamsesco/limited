import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/models/message.dart';
import 'package:limited/src/common/models/user.dart';
import 'package:limited/src/messenger/chat_item.dart';
import 'package:limited/src/services/settings_services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Chats extends StatefulWidget {
  Chats();

  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  List<Message> list = new List<Message>();
  Timer _time;
  var data;
  User user = new User("", "", "", "", DateTime.now(), "", "");
  int i = 0;

  var loading = false;

  Reload() {
    setState(() {
      loading = true;
    });
    new Timer(const Duration(seconds: 1), () {
      try {
        setState(() => loading = false);
      } catch (e) {
        e.toString();
      }
    });
  }

  Settings_Services userp = new Settings_Services();

  String id = "";

  getUserinfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (!this.mounted) return;
    setState(() {
      id = prefs.getString("id");
    });

    print("--------------------------------------");
    print(id);

    data = FirebaseDatabase.instance
        .reference()
        .child("room")
        .orderByChild("idUser")
        .equalTo(id);
    data.onValue.forEach((val) {
      try {
        setState(() {
          i = 1;
          if (val.snapshot.value == null) i = 2;
        });
      } catch (e) {}
    });

    var a = await userp.user_informations(id.toString());

    if (!this.mounted) return;
    setState(() {
      user = a;
    });
  }

  @override
  initState() {
    super.initState();

    getUserinfo();

    // FirebaseDatabase.instance.setPersistenceEnabled(true);
    // data.keepSynced(true);
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingScreen() {
      final ThemeData theme = Theme.of(context);
      return new Container(
          child: new Center(
              child: new Text("Loading...", style: theme.textTheme.display1)));
    }

    Widget emptyScreen() {
      final ThemeData theme = Theme.of(context);
      return new Container(
          child: new Center(
              child: new Text("No Messages", style: theme.textTheme.display1)));
    }

    Widget nomessagesfound = new Container(
        padding: new EdgeInsets.only(top: 24.0),
        child: new Center(child: new Text("No messages found")));

    Widget listmessages = id == ""
        ? Container()
        : new FirebaseAnimatedList(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            defaultChild: Theme(
                data: ThemeData(
                    cupertinoOverrideTheme:
                        CupertinoThemeData(brightness: Brightness.light)),
                child: CupertinoActivityIndicator()),
            padding: new EdgeInsets.all(4.0),
            query: data,
            sort: (a, b) =>
                b.value['timestamp'].compareTo(a.value['timestamp']),
            itemBuilder:
                (_, DataSnapshot snap, Animation<double> animation, int a) {
              return new ChatItem(snap, id, Reload, user);
            },
            duration: new Duration(milliseconds: 1000));

    return i == 2
        ? nomessagesfound
        : !loading
            ? Padding(
                padding: EdgeInsets.only(bottom: 88.h), child: listmessages)
            : new Container();
  }

  @override
  bool get wantKeepAlive => true;
}

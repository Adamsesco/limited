import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:limited/src/common/config/config.dart';
import 'package:limited/src/common/widgets/privacy.dart';
import 'package:limited/src/common/widgets/terms.dart';
import 'package:limited/src/otp/otp_verification.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:limited/src/services/rest_services.dart';
import 'package:limited/src/services/storage_util.dart';
import 'package:limited/src/services/validators.dart';
import 'package:limited/src/signin/signin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

Widget textfield(String name, TextEditingController texted, FocusNode focus,
        validator, TextInputType typeinput,
        {obs}) =>
    TextFormField(
        obscureText: obs == null ? false : obs,
        focusNode: focus,
        keyboardType: typeinput,
        decoration: InputDecoration(
          filled: true,
          fillColor: const Color(0xffFAFAFA),
          focusColor: const Color(0xffFAFAFA),
          hintText: name,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(
              color: const Color(0xffFAFAFA),
            ),
          ),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: const Color(0xffFAFAFA),
                width: 2.0,
              )),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
          ),
          // labelText: name,
          contentPadding:
              EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
        ),
        textAlignVertical: TextAlignVertical.top,
        minLines: 1,
        maxLines: 1,
        controller: texted,
        validator: validator);

class Register extends StatefulWidget {
  Register(this.phone);

  String phone;

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _usernameController = new TextEditingController();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  FocusNode _focususername = new FocusNode();

  FocusNode _focuspass = new FocusNode();
  FocusNode _focusname = new FocusNode();
  TextEditingController _emailController = new TextEditingController();

  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  RestService restservice = new RestService();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  AppService ap = new AppService();

  submit_name() {}

  submit_email() {}

  submit_tel() {}

  submit_password() {}

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() async {
    //var token = await _firebaseMessaging.getToken();

    final FormState form = _formKey.currentState;
    if (!form.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar("Correct ");
    } else {
      form.save();

      setState(() {
        show = true;
      });
      String name = _nameController.text;
      String _em = _emailController.text;
      String pass = _passwordController.text;
      String tel = _telController.text;

      String token = await _firebaseMessaging.getToken();


      var a = await restservice.post_register(
          "signup/",
          json.encode({
            "name": "$name",
            "username": "${_usernameController.text}",
            "password": "$pass",
            "phone": "$tel",
            "email": "$_em"
          }));

      setState(() {
        show = false;
      });
      if (a == 400) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text("exists".tr()),
          duration: new Duration(seconds: 4),
        ));
      } else if (a == 190) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: new Text('No internet connection !'),
          duration: new Duration(seconds: 4),
        ));
      } else {
        showInSnackBar("Sign up with success");
        //  SharedPreferences prefs = await SharedPreferences.getInstance();
        /*  prefs.setString("avatar", "https://res.cloudinary.com/dgxctjlpx/image/upload/v1618096841/placeholder_itp3gm.png");
        prefs.setString("username", _usernameController.text);
        prefs.setString("phone", _telController.text);*/

        StorageUtil.inicializar().then((_) {
          //var myProvider = Provider.of<LoginProvider>(context, listen: false);

          // myProvider.mitexto = 'Provider';
          //myProvider.isAuthenticate = true;

          //StorageUtil.putBool('isAutenticado', true);
          Timer(Duration(seconds: 1), () {
          //  Navigator.pop(context);
            Navigator.pop(context);
            Navigator.pop(context);

            Navigator.pushReplacement(context,
                new MaterialPageRoute(builder: (BuildContext context) {
              return Login(_telController.text);
            }));
            /*Navigator.push(context,
                new MaterialPageRoute(builder: (BuildContext context) {
                  return OtpVerification();
                }));*/
          });
        });
      }
    }
  }

  String token = "";

  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  Future<String> _fcmSetupAndGetToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await _firebaseMessaging.getToken().then((tk) {
      token = tk;
      prefs.setString("token", tk);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _telController.text = widget.phone;

    _fcmSetupAndGetToken();
  }

  @override
  Widget build(BuildContext context) {
    Widget textfield(String name, TextEditingController texted, FocusNode focus,
            validator, TextInputType typeinput,
            {obs}) =>
        TextFormField(
            obscureText: obs == null ? false : obs,
            focusNode: focus,
            keyboardType: typeinput,
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xffFAFAFA),
              focusColor: const Color(0xffFAFAFA),
              hintText: name,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: BorderSide(
                  color: const Color(0xffFAFAFA),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(25.0),
                  borderSide: BorderSide(
                    color: const Color(0xffFAFAFA),
                    width: 2.0,
                  )),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
              ),
              // labelText: name,
              contentPadding:
                  EdgeInsets.only(right: 8, left: 10, top: 15, bottom: 5),
            ),
            textAlignVertical: TextAlignVertical.top,
            minLines: 1,
            maxLines: 1,
            controller: texted,
            validator: validator);

    Widget textfield_name = textfield("Full name", _nameController, _focusname,
        ap.validatename, TextInputType.text);

    Widget textfield_username = textfield("Username", _usernameController,
        _focususername, ap.validatename, TextInputType.text);

    Widget textfield_password = textfield(
      "Password",
      _passwordController,
      _focuspass,
      ap.validatePassword,
      TextInputType.text,
      obs: true,
    );

    Widget textfield_email = textfield("Email", _emailController, _focusemail,
        ap.validateEmail, TextInputType.emailAddress);

    Widget textfield_tel = textfield("Mobile number", _telController, _focustel,
        ap.validatephonenumber, TextInputType.phone);

    TextStyle text1_btn = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w700);

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: ListView(
        padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
        children: <Widget>[
          Container(
            height: ScreenUtil().setHeight(24),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              )
            ],
          ),
          Container(
            height: ScreenUtil().setHeight(8),
          ),
          Container(
              width: ScreenUtil().setWidth(130),
              height: ScreenUtil().setHeight(130),
              child: SvgPicture.asset("assets/images/icons/logo.svg")),
          Container(
            height: ScreenUtil().setHeight(12),
          ),
          Center(
            child: Text(
              "Create account",

              ///إنشاء حساب
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(14),
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            height: ScreenUtil().setHeight(16),
          ),
          new Form(
            key: _formKey,
            autovalidate: _autovalidate,
            //onWillPop: _warnUserAboutInvalidData,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_name,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_username,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_password,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_email,
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(36),
                      right: ScreenUtil().setWidth(42),
                      top: ScreenUtil().setHeight(16)),
                  child: textfield_tel,
                ),
                Container(
                  height: ScreenUtil().setHeight(16),
                ),
                Container(
                    height: ScreenUtil().setHeight(42),
                    width: ScreenUtil().setWidth(185),
                    child: RaisedButton(
                        color: Config.col_blue,
                        elevation: 1,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(28),
                          ),
                        ),
                        onPressed: () {
                          _handleSubmitted();
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              show //CupertinoActivityIndicator()
                                  ? Theme(
                                      data: ThemeData(
                                          cupertinoOverrideTheme:
                                              CupertinoThemeData(
                                                  brightness: Brightness.dark)),
                                      child: CupertinoActivityIndicator())
                                  /*valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Colors.white),
                                    )*/
                                  : Container(),
                              show //CupertinoActivityIndicator()
                                  ? Container(
                                      width: 4,
                                    )
                                  : Container(),
                              Text(
                                "REGISTER",
                                style: text1_btn,
                              ),
                            ]))),
                /* Container(
                        child: GradientButton(
                      increaseWidthBy: MediaQuery.of(context).size.width,
                      increaseHeightBy: ScreenUtil().setHeight(40),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            show
                                ? Center(
                                    child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white),
                                  ))
                                : Container(),
                            Container(
                              width: 8,
                            ),
                            Text(
                              'دخول',
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            )
                          ]),
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                      ),
                      callback: () {
                        _handleSubmitted();
                      },

                    )),*/
              ],
            ),
          ),
          Container(
            height: ScreenUtil().setHeight(16),
          ),
          /*  Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: new RichText(
                    text: new TextSpan(
                      text: "بالتسجيل ، فأنت توافق على الالتزام بهذه  ",
                      style: new TextStyle(
                          color: Config.col_grey_medium,
                          fontFamily: "arb",
                          fontSize: ScreenUtil().setSp(9)),
                      children: <TextSpan>[
                        new TextSpan(
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Terms())),
                            text: "الشروط والأحكام ",
                            style: new TextStyle(
                                color: Config.col_blue,
                                fontWeight: FontWeight.bold)),
                        new TextSpan(
                            text: "  و على  ",
                            style:
                                new TextStyle(color: Config.col_grey_medium)),
                        new TextSpan(
                            text: " سياسة الخصوصية. ",
                            style: new TextStyle(
                                color: Config.col_blue,
                                fontWeight: FontWeight.bold),
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Privacy()))),
                      ],
                    ),
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "هل لديك حساب؟",
                style: TextStyle(color: Config.col_green),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "سجل دخولك",
                  style: TextStyle(
                      color: Config.col_blue, fontWeight: FontWeight.w700),
                ),
              )
            ],
          )*/
        ],
      ),
    );
  }
}

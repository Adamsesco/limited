import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:limited/src/home/bloc/bloc.dart';
import 'package:limited/src/home/data/home_repository.dart';
import 'package:limited/src/services/login_provider.dart';
import 'package:provider/provider.dart' as p;

import 'package:limited/src/main_screen.dart';

void main() async {
  final HomeRepository homerepository = new HomeRepository();

  runApp(MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (_) => HomeBloc(homeRepository: homerepository),
        ),
      ],
      child: EasyLocalization(
          supportedLocales: [Locale('en'), Locale('ar')],
          path: 'assets/translations',
          fallbackLocale: Locale('en'),
          startLocale: Locale('en'),
          useOnlyLangCode: true,
          child: MyApp())));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return p.MultiProvider(
        providers: [
          p.ChangeNotifierProvider(
            create: (_) => LoginProvider(),
          )
        ],
        child: MaterialApp(
          title: 'Limited',
          supportedLocales: context.supportedLocales,
          localizationsDelegates: context.localizationDelegates,
          debugShowCheckedModeBanner: false,
          locale: context.locale,
          theme: ThemeData(
              appBarTheme: AppBarTheme(brightness: Brightness.light),
              visualDensity: VisualDensity.adaptivePlatformDensity,
              fontFamily: "Roboto regular"),
          home: MainScreen(),
        ));
  }
}
